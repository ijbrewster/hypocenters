$(document).ready(function(){

$('#submit').click(getReport);

});

function getReport(){
    var dateFrom=$('#dateFrom').val()
    var dateTo=$('#dateTo').val()
    
    $.getJSON('/report/run',{'datefrom':dateFrom,
                             'dateto':dateTo})
    .done(function(data){
        $('#results').html(data);
    })
}