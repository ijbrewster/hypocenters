// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function (monQuakeList, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var sortOption = 'unixTime', sortAscDesc = 'desc', mobileContaierHeightWhenMap = 0.0;
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function renderHtmlTable() {

        if (sortAscDesc === 'asc') {
            monQuakeData.quakeRecs.sort(sortQuakeDataAsc);
        } else {
            monQuakeData.quakeRecs.sort(sortQuakeDataDesc);
        }

        $('.vscmm-quake-table tbody').empty();

        $('.vscmm-depth-units').html(persistentOptions.get('kmOrMi'));
        $('.vscmm-time-units').html(persistentOptions.get('utcOrLocal'));
        
        var filteredTotal = 0, visibleOnMapTotal = 0;
        $('#vscmmDesktopVisible_SPAN').empty();
                
        var rowsHtml = '', kmOrMi = persistentOptions.get('kmOrMi'), utcOrLocal = persistentOptions.get('utcOrLocal');
        
        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {

            var quakeRec = monQuakeData.quakeRecs[idx];
        
            if (quakeRec.isVisibleOnMap) { visibleOnMapTotal++; }
            if (quakeRec.isFiltered || !quakeRec.isVisibleOnMap) { continue; }

            filteredTotal++;
            
            var depthString = monUtils.round(monUtils.kmToMi(quakeRec.depthKM), 1);
            if (kmOrMi === 'km') { depthString = quakeRec.depthKM; }

            var timeString = quakeRec.quakeDateString;
            if (utcOrLocal === 'UTC') { timeString = quakeRec.quakeUTCString; }

            rowsHtml +=
                    '<tr class="vsc-pointer vscmm-data-row' + 
                            (parseFloat(quakeRec.mag) >= 3.5 ? ' vscmm-large-mag ' : '') + '" ' + 
                            'onclick="monQuakeUI.showDetail(\'' + quakeRec.eventId + '\')" ' +
                            'data-eventId="' + quakeRec.eventId + '">' + 
                    '    <td class="vscmm-date-string">' + timeString + '</td>' + 
                    '    <td class="vscmm-mag">' + quakeRec.mag + '</td>' + 
                    '    <td class="vscmm-depth-string">' + depthString + '</td>' + 
                    '</tr>'
                    ;
        }
        
        $('.vscmm-quake-table tbody').append(rowsHtml);
        
        $('.vscmm-quake-table tbody tr').hover(

            function (){ // Mouse enter
                monQuakeMap.activateQuake($(this).data('eventid'));
            },
            function (){ // Mouse out
                monQuakeMap.deactivateQuake();
            }
        );

        if (filteredTotal < visibleOnMapTotal) {
            $('#vscmmDesktopVisible_SPAN').html(
                (visibleOnMapTotal - filteredTotal) + ' Earthquakes are filtered. ' +
                '<button class="btn btn-primary btn-xs" onclick="monQuakeUI.showAll();">Show All</button><br/><br/>'
            );
        }
    }
        
    function sortQuakeDataDesc(a, b) {
        
        if (parseFloat(a[sortOption]) > parseFloat(b[sortOption])) { return -1; }
        if (parseFloat(a[sortOption]) < parseFloat(b[sortOption])) { return 1; }
        return 0;
    }

    function sortQuakeDataAsc(a, b) {
        
        if (parseFloat(a[sortOption]) < parseFloat(b[sortOption])) { return -1; }
        if (parseFloat(a[sortOption]) > parseFloat(b[sortOption])) { return 1; }
        return 0;
    }

    function renderRowsAndCols() {

        if (sortAscDesc === 'asc') {
            monQuakeData.quakeRecs.sort(sortQuakeDataAsc);
        } else {
            monQuakeData.quakeRecs.sort(sortQuakeDataDesc);
        }
        
        $('#vscmmQuakeListMobile').empty();
        
        $('#vscmmQuakeListMobile').append('<span id="vscmmMobileVisible_SPAN"></span>');
        
        $('.vscmm-depth-units').html(persistentOptions.get('kmOrMi'));
        $('.vscmm-time-units').html(persistentOptions.get('utcOrLocal'));

        var filteredTotal = 0, visibleOnMapTotal = 0, rowsHtml = '';
        
        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {

            var quakeRec = monQuakeData.quakeRecs[idx];
        
            if (quakeRec.isVisibleOnMap) { visibleOnMapTotal++; }
            if (quakeRec.isFiltered || !quakeRec.isVisibleOnMap) { continue; }

            filteredTotal++;

            var depthString = monUtils.round(monUtils.kmToMi(quakeRec.depthKM), 1);
            if (persistentOptions.get('kmOrMi') === 'km') { depthString = quakeRec.depthKM; }

            var timeString = quakeRec.quakeDateString;
            if (persistentOptions.get('utcOrLocal') === 'UTC') { timeString = quakeRec.quakeUTCString; }

            rowsHtml += 
                '<div class="vsc-pointer row hoverDiv ' + 
                    (parseFloat(quakeRec.mag) >= 3.5 ? ' vscmm-large-mag ' : '') + '"' + 
                    ' onclick="monQuakeUI.showDetail(\'' + quakeRec.eventId + '\')" ' +
                '> ' +
                '    <div class="col col-xs-6">' + timeString + '</div> ' +
                '    <div class="col col-xs-3">' + quakeRec.mag + '</div> ' +
                '    <div class="col col-xs-3">' + depthString + '</div> ' +
                '</div> '
                ;
        }
        
        $('#vscmmQuakeListMobile').html(rowsHtml);
        
        if (filteredTotal < visibleOnMapTotal) {
            $('#vscmmMobileVisible_SPAN').html(
                '<div class="well well-sm">' +
                (visibleOnMapTotal - filteredTotal) + ' Earthquakes are filtered. ' +
                '<button class="btn btn-primary btn-xs" onclick="monQuakeUI.showAll();">Show All</button>' + 
                '</div>'
            );
        }
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monQuakeList.renderRowsAndCols = function() {
        
        renderRowsAndCols();
    };
    
    monQuakeList.renderHtmlTable = function () {

        renderHtmlTable();
    };
    
    monQuakeList.tablerowActive = function(eventId) {
        
        $('.vscmm-quake-table tr').removeClass('active');
        $('.vscmm-quake-table tr[data-eventId="' + eventId + '"]').addClass('active');
        /* 
        // TODO: use this pattern to determine better scroll functionality.
        var quake3dBoxCoordinates = document.getElementById('quake-3d').getBoundingClientRect();
        if (
            event.clientX >= quake3dBoxCoordinates.left && event.clientX <= quake3dBoxCoordinates.right &&
            event.clientY >= quake3dBoxCoordinates.top && event.clientY <= quake3dBoxCoordinates.bottom
            ) {
            isMouseInQuake3dBox = true;
        } else {
            isMouseInQuake3dBox = false;
        }
        */
       /*
        $('.vscmm-quake-table-container').stop().animate({
            scrollTop: $('.vscmm-quake-table tr[data-eventId="' + eventId + '"]').offset().top
        }, 500);
        */
    };
    
    monQuakeList.tablerowInactive = function() {
        
        $('.vscmm-quake-table tr').removeClass('active');
    };

    monQuakeList.sortClicked = function(sortOptionIn) {
        
        if (sortOption !== sortOptionIn) {
            sortOption = sortOptionIn;
            sortAscDesc = 'asc';
        }

        if (sortAscDesc === 'asc') {
            sortAscDesc = 'desc';
        } else {
            sortAscDesc = 'asc';
        }
        
        monQuakeList.sort();
    };

    monQuakeList.sort = function() {

        if (sortAscDesc === 'asc') {
            monQuakeData.quakeRecs.sort(sortQuakeDataAsc);
        } else {
            monQuakeData.quakeRecs.sort(sortQuakeDataDesc);
        }

        if (monMain.isDesktop) {
            renderHtmlTable();
        } else {
            renderRowsAndCols();
        }
    };

    monQuakeList.showHideMobile = function() {

        // The view list button shows quakes if off.
        if ($('.vscmm-btn-quake-show').is(':visible')) { monQuakeUI.showAll(); }

        if ($('#vscmmMapContainerMobile').is(':visible')) {
            
            $('#vscmmMapContainerMobile').hide();
            $('#vscmmQuakeListMobileContainer').show();
            $('.quake-list-visible').show();
            $('.quake-list-hidden').hide();
            mobileContaierHeightWhenMap = $('#vscmmMobileContainer').outerHeight();
            $('#vscmmMobileContainer').height($('#vscmmQuakeListMobileContainer').outerHeight() + 100);
            
            monUI.toggleLegend(false);
            $('#vscmmLegendShowButton').hide();
            
        } else {
            
            $('#vscmmMapContainerMobile').show();
            $('#vscmmQuakeListMobileContainer').hide();
            $('.quake-list-visible').hide();
            $('.quake-list-hidden').show();
            $('#vscmmMobileContainer').height(mobileContaierHeightWhenMap);
            
            $('#vscmmLegendShowButton').show();
        }
    };

}(window.monQuakeList = window.monQuakeList || {}, jQuery));
