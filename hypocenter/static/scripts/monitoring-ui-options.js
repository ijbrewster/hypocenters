/* global monitoringOptions */

// All application options that affect the user should be processed here. We want to read in options from the exterior
// monitoringOptions object first, then process any applicable persistent options, then any url parameters.
// This should apply equally to mobile and desktop versions.

(function (monUiOptions, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // These are the defaults unless overridden by exterior elements.
    monUiOptions.title               = 'Earthquakes';
    monUiOptions.isDesktop           = true;
    monUiOptions.vnum                = 0;
    monUiOptions.centerLat           = 0.0;
    monUiOptions.centerLong          = 0.0;
    monUiOptions.mapZoom             = 13;
    monUiOptions.zoomThreshold       = 8;
    monUiOptions.showBoundariesOnMap = true;
    monUiOptions.basemap             = 'topo-vector';
    monUiOptions.showInstruments     = false;
    monUiOptions.showSeismic         = true;
    monUiOptions.monitoringPath      = monUtils.getMonitoringPath();
    monUiOptions.eventId             = '';
    
    // Quakes JSON data sources
    monUiOptions.quakesUrl           = 'get_quake_data';
    // Quakes documentation: https://earthquake.usgs.gov/fdsnws/event/1/
    // Example quake calls:
    // https://earthquake.usgs.gov/fdsnws/event/1/query.geojson?maxlatitude=19.53&minlatitude=19.30&maxlongitude=-155.16&minlongitude=-155.40&orderby=time
    // https://earthquake.usgs.gov/fdsnws/event/1/query?eventid=hv70024152&format=geojson

    // Instruments JSON data sources
    monUiOptions.instrumentsUrl      = 'https://volcanoes.usgs.gov/vsc/api/instrumentApi/data';
    // Example instrument call:
    // https://volcanoes.usgs.gov/vsc/api/instrumentApi/data?lat1=40&lat2=44&long1=-124&long2=-120

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    function processExteriorOptions() {
        
        if (typeof monitoringOptions === 'undefined') { return; }

        if (monUtils.objectHasProperty(monitoringOptions, 'vnum') && monitoringOptions.vnum.length > 0) {
            monUiOptions.vnum = monitoringOptions.vnum;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'quakeDetailUrl') && 
                monitoringOptions.quakeDetailUrl.trim() !== '') {
            monUiOptions.quakeDetailUrl = monitoringOptions.quakeDetailUrl;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'quakesUrl') && monitoringOptions.quakesUrl.trim() !== '') {
            monUiOptions.quakesUrl = monitoringOptions.quakesUrl;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'instrumentsUrl') && 
                monitoringOptions.instrumentsUrl.trim() !== '') {
            monUiOptions.instrumentsUrl = monitoringOptions.instrumentsUrl;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'monitoringPath') && 
                monitoringOptions.monitoringPath.trim() !== '') {
            monUiOptions.monitoringPath = monitoringOptions.monitoringPath;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'showBoundariesOnMap') && 
                monitoringOptions.showBoundariesOnMap) {
            monUiOptions.showBoundariesOnMap = monitoringOptions.showBoundariesOnMap;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'basemap') && monitoringOptions.basemap.trim() !== '') {
            monUiOptions.basemap = monitoringOptions.basemap;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'centerLat')) {
            monUiOptions.centerLat = monitoringOptions.centerLat;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'centerLong')) {
            monUiOptions.centerLong = monitoringOptions.centerLong;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'showInstruments')) {
            monUiOptions.showInstruments = monitoringOptions.showInstruments;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'mapZoom') && monitoringOptions.mapZoom !== '') {
            monUiOptions.mapZoom = monitoringOptions.mapZoom;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'zoomThreshold') && monitoringOptions.zoomThreshold !== '') {
            monUiOptions.zoomThreshold = monitoringOptions.zoomThreshold;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'title') && monitoringOptions.title.trim() !== '') {
            monUiOptions.title = monitoringOptions.title;
        }
        
        if (monUtils.objectHasProperty(monitoringOptions, 'vnum') && monitoringOptions.vnum.trim() !== '') {
            monUiOptions.vnum = monitoringOptions.vnum;
        }
    }
    
    function processPersistentOptions() {
        
        persistentOptions.restore();
    }
    
    function processUrlParameters() {
        
        if (monUtils.getURLParameter('vnum')) { monUiOptions.vnum = monUtils.getURLParameter('vnum'); }
        if (monUtils.getURLParameter('mapZoom')) { monUiOptions.mapZoom = monUtils.getURLParameter('mapZoom'); }
        if (monUtils.getURLParameter('centerLat')) { monUiOptions.centerLat = monUtils.getURLParameter('centerLat'); }
        if (monUtils.getURLParameter('centerLong')) { monUiOptions.centerLong = monUtils.getURLParameter('centerLong'); }
        if (monUtils.getURLParameter('eventid')) { monUiOptions.eventId = monUtils.getURLParameter('eventid'); }
        if (monUtils.getURLParameter('eventId')) { monUiOptions.eventId = monUtils.getURLParameter('eventId'); }
    }
    
    function prioritizeOptions() {
        
        // if vnum use vnum's lat, long
        if (monUiOptions.vnum && parseInt(monUiOptions.vnum, 10) > 0) {
            monVolcData.getVolcano(monUiOptions.vnum, function () {
                var volcRec = monVolcData.volcanoRecs[monUiOptions.vnum];
                monUiOptions.centerLong = volcRec.longitude;
                monUiOptions.centerLat = volcRec.latitude;
            });
        }
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    monUiOptions.init = function() {
        
        processExteriorOptions();
        processPersistentOptions();
        processUrlParameters();
        prioritizeOptions();
    };

}(window.monUiOptions = window.monUiOptions || {}, jQuery));
