// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri, dojo */
(function (monVolcMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var colorCode = '', alertLevel = '', volcanoName = '', iconName = '', point = {}, symbol = {}, 
        attribs = {}, graphic = {}, volcanoTemplate = {}, font = {}, textSymbol = {}, textGraphic = {};
    
    var icons = {
            'fields':                {'width':12,'height':12},
            'observatory':           {'width':21,'height':16},
            'unassigned_unassigned': {'width':18,'height':14},
            'green_normal':          {'width':21,'height':16},
            'yellow_advisory':       {'width':27,'height':20},
            'yellow_watch':          {'width':27,'height':20},
            'orange_watch':          {'width':33,'height':24},
            'orange_warning':        {'width':33,'height':24},
            'red_warning':           {'width':40,'height':31},
            'red_watch':             {'width':40,'height':31}
        };
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    monVolcMap.unassignedLayer;
    monVolcMap.greenLayer;
    monVolcMap.yellowLayer;
    monVolcMap.orangeLayer;
    monVolcMap.redLayer;
    monVolcMap.elevatedNamesLayer;
    monVolcMap.unElevatedNamesLayer;
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function addVolcano(vRec) {

        colorCode = vRec.colorCode.toLowerCase();
        alertLevel = vRec.alertLevel.toLowerCase();
        volcanoName = encodeURIComponent(vRec.vName);

        if (vRec.vnum === '332000') { volcanoName = 'L&#333;&#8216;ihi'; } // Loihi
        else if (vRec.vnum === '332010') { volcanoName = 'K&#299;lauea'; } // Kilauea
        else if (vRec.vnum === '332040') { volcanoName = 'Hual&#257;lai'; } // Hualalai 
        else if (vRec.vnum === '332060') { volcanoName = 'Haleakal&#257'; } // Haleakala
        else if (vRec.vnum === '322080') { volcanoName = 'Three Sisters'; }
        
        volcanoName = monUtils.decodeHtml(volcanoName);

        iconName = colorCode + '_' + alertLevel;

        point = new esri.geometry.Point(vRec.long, vRec.lat);
        point = esri.geometry.geographicToWebMercator(point);
        symbol = new esri.symbol.PictureMarkerSymbol(
                    'https://volcanoes.usgs.gov/images/icons/map/' + iconName + '.png', 
                    icons[iconName].width, icons[iconName].height
                );
        attribs = {
            'volcano_name' : volcanoName,
            'href'         : vRec.vUrl,
            'cc'           : vRec.colorCode,
            'al'           : vRec.alertLevel,
            'region'       : vRec.region,
            'vimg'         : vRec.vImage,
            'graphicType'  : 'volcano',
            'hoverText'    : volcanoName
            };

        graphic = new esri.Graphic(point, symbol, attribs);
        
        font = new esri.symbol.Font('10pt', esri.symbol.Font.STYLE_ITALIC, esri.symbol.Font.VARIANT_NORMAL, 
                                        esri.symbol.Font.WEIGHT_BOLD, 'Arial');
        textSymbol = new esri.symbol.TextSymbol(volcanoName, font, new dojo.Color('black'))
        	.setOffset(0, 10)
        	.setAlign(esri.symbol.Font.ALIGN_START);

        textGraphic = new esri.Graphic(point, textSymbol, attribs, volcanoTemplate);

        switch(colorCode) {
            case 'green':
                monVolcMap.greenLayer.add(graphic);
                monVolcMap.unElevatedNamesLayer.add(textGraphic);
                break;
            case 'yellow': 
                monVolcMap.yellowLayer.add(graphic);
                monVolcMap.elevatedNamesLayer.add(textGraphic);
                break;
            case 'orange':
                monVolcMap.orangeLayer.add(graphic);
                monVolcMap.elevatedNamesLayer.add(textGraphic);
                break;
            case 'red':
                monVolcMap.redLayer.add(graphic);
                monVolcMap.elevatedNamesLayer.add(textGraphic);
                break;
            default:
                monVolcMap.unassignedLayer.add(graphic);
                monVolcMap.unElevatedNamesLayer.add(textGraphic);
        }
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monVolcMap.addMapLayers = function () {

        monVolcMap.unassignedLayer = new esri.layers.GraphicsLayer();
        monVolcMap.greenLayer = new esri.layers.GraphicsLayer();
        monVolcMap.yellowLayer = new esri.layers.GraphicsLayer();
        monVolcMap.orangeLayer = new esri.layers.GraphicsLayer();
        monVolcMap.redLayer = new esri.layers.GraphicsLayer();
        monVolcMap.elevatedNamesLayer = new esri.layers.GraphicsLayer();
        monVolcMap.unElevatedNamesLayer = new esri.layers.GraphicsLayer();
    
        volcanoTemplate = new esri.InfoTemplate('${volcano_name}', '<a href="${href}">More Information</a>');

        monMap.map.addLayer(monVolcMap.unassignedLayer);
        dojo.connect(monVolcMap.unassignedLayer, 'onMouseOver', monMap.mapMouseOver);
        dojo.connect(monVolcMap.unassignedLayer, 'onMouseOut', monMap.mapMouseOut);
    
        monMap.map.addLayer(monVolcMap.greenLayer);
        dojo.connect(monVolcMap.greenLayer, 'onMouseOver', monMap.mapMouseOver);
        dojo.connect(monVolcMap.greenLayer, 'onMouseOut', monMap.mapMouseOut);
        
        monMap.map.addLayer(monVolcMap.unElevatedNamesLayer);
        
        monMap.map.addLayer(monVolcMap.yellowLayer);
        dojo.connect(monVolcMap.yellowLayer, 'onMouseOver', monMap.mapMouseOver);
        dojo.connect(monVolcMap.yellowLayer, 'onMouseOut', monMap.mapMouseOut);
        
        monMap.map.addLayer(monVolcMap.orangeLayer);
        dojo.connect(monVolcMap.orangeLayer, 'onMouseOver', monMap.mapMouseOver);
        dojo.connect(monVolcMap.orangeLayer, 'onMouseOut', monMap.mapMouseOut);
        
        monMap.map.addLayer(monVolcMap.redLayer);
        dojo.connect(monVolcMap.redLayer, 'onMouseOver', monMap.mapMouseOver);
        dojo.connect(monVolcMap.redLayer, 'onMouseOut', monMap.mapMouseOut);
        
        monMap.map.addLayer(monVolcMap.elevatedNamesLayer);

        monUI.toggleVolcanoes(persistentOptions.get('volcanoSetting'));
    };

    monVolcMap.showHideVolcNamesBasedOnZoom = function (mapZoom) {
        if (persistentOptions.get('volcanoSetting')=='off'){
            monVolcMap.elevatedNamesLayer.hide();
            monVolcMap.unElevatedNamesLayer.hide();
        }
        else if (parseInt(mapZoom, 10) >= 9) {
            monVolcMap.elevatedNamesLayer.show();
            monVolcMap.unElevatedNamesLayer.show();
        } else {
            monVolcMap.elevatedNamesLayer.show();
            monVolcMap.unElevatedNamesLayer.hide();
        }
    };
    
    monVolcMap.addOrRefreshVolcanoes = function(callback) {
        
        monVolcMap.unassignedLayer.clear();
        monVolcMap.greenLayer.clear();
        monVolcMap.yellowLayer.clear();
        monVolcMap.orangeLayer.clear();
        monVolcMap.redLayer.clear();
        monVolcMap.elevatedNamesLayer.clear();
        monVolcMap.unElevatedNamesLayer.clear();

        for ( var idx = 0; idx < monVolcData.regionRecs.length; idx++ ) {
            var vRec = monVolcData.regionRecs[idx];
            if (vRec.vnum === '322070') { continue; } // North Sister Field is now combined into Three Sisters.
            addVolcano(vRec);
        }
        
        if (typeof callback === 'function') { callback(); }
    };

}(window.monVolcMap = window.monVolcMap || {}, jQuery));
