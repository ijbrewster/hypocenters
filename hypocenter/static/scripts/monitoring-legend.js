// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monLegend, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function renderCircle(context, x, y, radius, color) {

        context.beginPath();
        context.arc(x, y, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();
        
    }
    
    function drawLine(context, x1, y1, x2, y2, color) {
        
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = color;
        context.moveTo(x1, y1); // top left
        context.lineTo(x2, y2);
        context.stroke();
    }
    
    function drawFilledRectWBorder(context, x, y, width, height, color) {

        context.beginPath();
        context.lineWidth = '1';
        
        context.fillStyle = color; // Rectangle fill color
        context.fillRect(x, y, width, height);
        
        context.strokeStyle = 'black';  // Rectangle border color
        context.rect(x, y, width, height);
        
        context.stroke();
    }
    
    function drawText(context, text, x, y, textAlign) {
        
        context.beginPath();
        context.fillStyle = 'black';
        context.strokeStyle = 'black';
        //context.font = 'bold 16px sans-serif';
        context.font = 'normal 12px sans-serif';
        context.textAlign = textAlign;
        context.fillText(text, x, y);
        context.stroke();
    }
    
    function renderQuakeDepthChart() {

        $('#vscmmLegendColors').empty();
                
        $('#vscmmLegendColors').each(function(index) {

            $(this).html(
                '<b>Depths</b><br/><canvas id="vscmmMagDepthCanvas' + index + '" width="140" height="160"></canvas>'
                );
            
            var canvas = document.getElementById('vscmmMagDepthCanvas' + index),
                context = canvas.getContext('2d'), 
                colorLeft = 5, colorRight = 70, RECT_HEIGHT = 20;

            context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.

            drawText(context, 'color', colorLeft, 12, 'left');
            drawText(context, persistentOptions.get('kmOrMi'), colorRight + 10, 12, 'left');

            // < 0
            drawFilledRectWBorder(context, colorLeft, 1 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(-5));
            drawText(context, '< 0', colorRight + 10, (1 * RECT_HEIGHT) + 15, 'left');

            // 0 - 5
            drawFilledRectWBorder(context, colorLeft, 2 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(0));
            drawText(
                    context, 
                    (persistentOptions.get('kmOrMi') === 'km' ? '0 - 5' : '0 - 3.1'), 
                        colorRight + 10, (2 * RECT_HEIGHT) + 15, 'left'
                );
            
            // 5 - 10
            drawFilledRectWBorder(context, colorLeft, 3 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(5));
            drawText(
                    context, 
                    (persistentOptions.get('kmOrMi') === 'km' ? '5 - 10' : '3.1 - 6.2'), 
                        colorRight + 10, (3 * RECT_HEIGHT) + 15, 'left'
                );

            // 10 - 15
            drawFilledRectWBorder(context, colorLeft, 4 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(10));
            drawText(
                    context, 
                    (persistentOptions.get('kmOrMi') === 'km' ? '10 - 15' : '6.2 - 9.3'), 
                        colorRight + 10, (4 * RECT_HEIGHT) + 15, 'left'
                );

            // 15 - 20
            drawFilledRectWBorder(context, colorLeft, 5 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(15));
            drawText(
                    context, 
                    (persistentOptions.get('kmOrMi') === 'km' ? '15 - 20' : '9.3 - 12.4'), 
                        colorRight + 10, (5 * RECT_HEIGHT) + 15, 'left'
                );

            // 20 - 40
            drawFilledRectWBorder(context, colorLeft, 6 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(20));
            drawText(
                    context, 
                    (persistentOptions.get('kmOrMi') === 'km' ? '20 - 40' : '12.4 - 24.9'), 
                        colorRight + 10, (6 * RECT_HEIGHT) + 15, 'left'
                );

            // > 40
            drawFilledRectWBorder(context, colorLeft, 7 * RECT_HEIGHT, 60, RECT_HEIGHT, monUtils.getDepthColor(40));
            drawText(context, (persistentOptions.get('kmOrMi') === 'km' ? '> 40' : '> 24.9'), 
                                    colorRight + 10, (7 * RECT_HEIGHT) + 15, 'left');
        });
    }
    
    function renderQuakeMagChart() {
        
        $('#vscmmLegendMagnitudes').empty();
                
        $('#vscmmLegendMagnitudes').each(function(index) {

            $(this).html(
                    '<b>Magnitudes</b><br/><canvas id="vscmmMagLegendCanvas' + index + '" width="100" height="130">' + 
                    '</canvas>'
                );
            var canvas = document.getElementById('vscmmMagLegendCanvas' + index),
                context = canvas.getContext('2d');

            context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.
            var col1 = 0, col2 = 60, pxRadius = 0.0, y = 0, col1C = ((col2 - col1) / 2) - 6;
            for (var mag = 0; mag <= 7; mag++) {
                pxRadius = monUtils.getQuakePixelDiameter(mag) / 2;
                y = 14 + (mag * 14);
                renderCircle(context, col1C, y - pxRadius, pxRadius, 'white');
                drawText(context, mag, col2, y, 'left');
            }
        });
    }

    function renderQuakeTimeChart() {

        $('#vscmmLegendColors').empty();
                
        $('#vscmmLegendColors').each(function(index) {

            $(this).html(
                    '<b>Times</b><br/><canvas id="vscmmMagTimeCanvas' + index + '" width="110" height="100"></canvas>'
                );
            var canvas = document.getElementById('vscmmMagTimeCanvas' + index),
                context = canvas.getContext('2d'),
                col1 = 10, col2 = 22, pxRadius = 6;

            context.translate(0.5, 0.5); // Fixes issues with fuzzy lines.
            
            renderCircle(context, col1, 10, pxRadius, monUtils.getTimeColor(1 / 24));
            drawText(context, 'Last 2 Hours', col2, 14, 'left');
                        
            renderCircle(context, col1, 30, pxRadius, monUtils.getTimeColor(1));
            drawText(context, 'Last 2 Days', col2, 34, 'left');
                        
            renderCircle(context, col1, 50, pxRadius, monUtils.getTimeColor(10));
            drawText(context, 'Last 2 Weeks', col2, 54, 'left');
                        
            renderCircle(context, col1, 70, pxRadius, monUtils.getTimeColor(20));
            drawText(context, '2+ Weeks', col2, 74, 'left');
        });
    }

    function renderInstCats() { // Called after instrument data is loaded.

        $('#vscmmLegendInstruments').empty();
         
        if (monInstrumentUI.getVisibleTotal() === 0){ return; }
        
        $('#vscmmLegendInstruments').append('<b>Instruments</b><br/>');
        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {
            
            var catRec = monInstrumentData.instrumentCategories[idx];
            if (!monInstrumentUI.isCategoryVisible(catRec.catId)) { continue; }
            $('#vscmmLegendInstruments').append(
                    '<div class="vscmm-left">' + 
                    '<img alt="Instrument Icon" src="' + catRec.iconUrl + '"/> ' + 
                    catRec.category + '</div><br/>'
            );
        }
    }
    
    function renderVolcanoes() {

        $('#vscmmLegendVolcanoes').empty();

        var volcLegendRecs = [], included = [];
        var sortOrder= {
            'unassigned_unassigned.png' : 1,
            'green_normal.png'          : 2,
            'yellow_advisory.png'       : 3,
            'orange_watch.png'          : 4,
            'orange_warning.png'        : 5,
            'red_watch.png'             : 6,
            'red_warning.png'           : 7
        };

        for (var idx = 0; idx < monVolcData.regionRecs.length; idx++) {
            
            var volcRec = monVolcData.regionRecs[idx],
                iconName = volcRec.colorCode.toLowerCase() + '_' + volcRec.alertLevel.toLowerCase() + '.png';
            if (included.indexOf(iconName) > -1) { continue; }
            included.push(iconName);
            volcLegendRecs.push({
                'sortOrder' : sortOrder[iconName],
                'iconName'  : iconName, 
                'iconText'  : volcRec.alertLevel + '/' + volcRec.colorCode
            });
        }

        if (included.length === 0) { return; }

        volcLegendRecs.sort(function(a, b) { return a['sortOrder'] > b['sortOrder']; });

        $('#vscmmLegendVolcanoes').append('<b>Volcanoes</b><br/>');
        for (var idx = 0; idx < volcLegendRecs.length; idx++) {
            
            var iconUrl = 'https://volcanoes.usgs.gov/images/icons/map/' + volcLegendRecs[idx].iconName;
          
            $('#vscmmLegendVolcanoes').append(
                    '<div class="vscmm-left"><img alt="Volcano status icon." src="' + iconUrl + '" height="12px"/> ' + 
                    volcLegendRecs[idx].iconText + '</div><br/>'
            );
        }
    }
    
    function renderTypes(){
        const div=$('#vscmmLegendTypes').empty()
        div.append('<b>Reviewed</b></br>')
        div.append(
            '<div class="vscmm-left"><span class="legendSymbol">&#9670;</span> AUTOMATIC, Unreviewed</div><br/>'
        )
        div.append(
            '<div class="vscmm-left"><span class="legendSymbol">&#9679;</span> Reviewed</div><br/>'
        )
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monLegend.renderInstCats = function() { renderInstCats(); };
    
    monLegend.renderVolcanoes = function() { renderVolcanoes(); };
    monLegend.renderTypes = function() { renderTypes(); };
    
    monLegend.renderQuakeInfo = function() {
        renderQuakeMagChart();
        persistentOptions.get('quakeColorChoice') === 'time' ? renderQuakeTimeChart() : renderQuakeDepthChart();
    };
    
}(window.monLegend = window.monLegend || {}, jQuery));
