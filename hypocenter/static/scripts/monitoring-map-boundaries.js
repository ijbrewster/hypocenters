// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri, dojo */
(function (monMapBoundaries, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var boundariesData = {},
        boundariesDownloaded = false;
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monMapBoundaries.boundariesLayer;

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function addBoundaries() {
        
        for (var idx = 0; idx < boundariesData.length; idx++) {
            
            var b1 = {}, p1 = {}, attribs = { "hoverText": boundariesData[idx].title };

            b1 = new esri.symbol.SimpleLineSymbol(
                esri.symbol.SimpleLineSymbol.STYLE_SOLID,  // Line Style
                new dojo.Color(boundariesData[idx].color), // Color
                2                                          // Width
            );

            p1 = new esri.geometry.Polygon(new esri.SpatialReference({wkid: 4326}));
            p1.addRing(boundariesData[idx].polygon);
            monMapBoundaries.boundariesLayer.add(new esri.Graphic(p1, b1, attribs));
        }
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monMapBoundaries.addBoundaries = function () {

        if (boundariesDownloaded) { return; }

        $.getJSON(monUiOptions.monitoringPath + 'scripts/json/boundaries.json', function(data) {

            boundariesDownloaded = true;
            boundariesData = data;
            monMapBoundaries.boundariesLayer = new esri.layers.GraphicsLayer();
            monMap.map.addLayer(monMapBoundaries.boundariesLayer);
            dojo.connect(monMapBoundaries.boundariesLayer, 'onMouseOver', monMap.mapMouseOver);
            dojo.connect(monMapBoundaries.boundariesLayer, 'onMouseOut', monMap.mapMouseOut);
            addBoundaries();
        });
    };

}(window.monMapBoundaries = window.monMapBoundaries || {}, jQuery));
