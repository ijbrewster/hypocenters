// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function (monVolcData, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////
    
    var apiUrl = 'https://volcanoes.usgs.gov/vsc/api/volcanoApi', prevParams = '';
    // Example region API call: 
    //     https://volcanoes.usgs.gov/vsc/api/volcanoApi/regionstatus?lat1=18.46&long1=-156.91&lat2=20.77&long2=-153.98
    // Example volcano API call: https://volcanoes.usgs.gov/vsc/api/volcanoApi/volcano?vnum=332010

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monVolcData.volcanoRecs = {};
    monVolcData.regionRecs = [];
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function getRegion(minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {
        
        var params = '?lat1=' + minLatIn + '&long1=' + minLongIn + '&lat2=' + maxLatIn + '&long2=' + maxLongIn;    
        $.getJSON(apiUrl + '/regionstatus' + params, function(data) {
            monVolcData.regionRecs = monVolcData.regionRecs.concat(data);
            if (typeof callback === 'function') { callback(); }
        });
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monVolcData.getVolcano = function (vnum, callback) {

        if (monVolcData.volcanoRecs[vnum]) {

            if (typeof callback === 'function') { callback(); }

        } else {

            $.getJSON(apiUrl + '/volcano?vnum=' + vnum, function(data) {
                monVolcData.volcanoRecs[vnum] = data;
                if (typeof callback === 'function') { callback(); }
            });
        }
    };
    
    monVolcData.getRegion = function (minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {

        var params = '?lat1=' + minLatIn + '&long1=' + minLongIn + '&lat2=' + maxLatIn + '&long2=' + maxLongIn;
        if (params === prevParams) { return; }
        prevParams = params;

        monVolcData.regionRecs = [];
        getRegion(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {
            
            if (minLongIn < -180 || maxLongIn < -180) { // Crossing the date line, moving west. Do second data pull.

                if (minLongIn < -180 && maxLongIn > -180) {
                    maxLongIn = 180;
                    minLongIn = minLongIn + 360;
                } else {
                    if (minLongIn < -180) { minLongIn = minLongIn + 360; }
                    if (maxLongIn < -180) { maxLongIn = maxLongIn + 360; }
                    if (maxLongIn < minLongIn) {
                        var tmp = maxLongIn;
                        maxLongIn = minLongIn;
                        minLongIn = tmp;
                    }
                }

                getRegion(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {
                    if (typeof callback === 'function') { callback(); }
                });
                
            } else {

                if (typeof callback === 'function') { callback(); }
            }
        });
    };
    
    
}(window.monVolcData = window.monVolcData || {}, jQuery));
