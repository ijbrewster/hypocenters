// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global esri */
(function (endUser, $, undefined) {

    "use strict";

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var apiUrl = 'https://volcanoes.usgs.gov/vsc/api/endUserApi/';
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    endUser.coords = {};
    endUser.ip = '';
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function setCoordsFromIp(callback) {

        $.getJSON(apiUrl + 'geoIp', function(data) { //$.getJSON(apiUrl + 'geoIp/' + endUser.ip, function(data) {
            if (data.latitude && data.longitude) {
                endUser.coords.latitude = parseFloat(data.latitude);
                endUser.coords.longitude = parseFloat(data.longitude);
            }
            if (typeof callback === 'function') {
                 callback(); 
            }
        });
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    endUser.setPosition = function(callback) {

        if (window.location.protocol === "https:" && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                endUser.coords.latitude = parseFloat(position.coords.latitude);
                endUser.coords.longitude = parseFloat(position.coords.longitude);
                if (typeof callback === 'function') { callback(); }
            }, function(error) { // Coord errors.
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        console.log('User denied the request for Geolocation.');
                        break;
                    case error.POSITION_UNAVAILABLE:
                        console.log('Location information is unavailable.');
                        break;
                    case error.TIMEOUT:
                        console.log('The request to get user location timed out.');
                        break;
                    case error.UNKNOWN_ERROR:
                        console.log('An unknown error occurred.');
                        break;
                }
                setCoordsFromIp(callback);
            });
            if (typeof callback === 'function') { callback(); }
        } else {
            setCoordsFromIp(callback);
        }
    };
       
    endUser.setIp = function(callback) {
        
        $.getJSON(apiUrl + 'clientIp', function(data) {
            endUser.ip = data.ip;
            if (typeof callback === 'function') { callback(); }
        });
    }
       
    endUser.init = function (){
        
        // endUser.setIp();
        // endUser.setPosition();
        
        /*
        $.getJSON(apiUrl + 'clientIp', function(data) {
            
            endUser.ip = data.ip;
            setCoordsFromIp(endUser.ip);

            if (window.location.protocol === "https:" && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(setCoords);
            } else {
                setCoordsFromIp(endUser.ip);
            }

        });
         */
    };
    
}(window.endUser = window.endUser || {}, jQuery));

$(document).ready(function (){
    endUser.init();
});