var page = 0

$(document).ready(function() {
    $('#refresh').click(function() { get_data() });
    $('#numWeeks').change(checkWeekRange);
    $('#legend').click(show_legend);
    $('#locView').change(function() {
        var range = this.value;
        get_data(range);
    });
    get_data();
    $(document).on('click', '.nextPage', nextPage);
    $(document).on('click', '.prevPage', prevPage);
    $(document).on('keypress', ".strictnumsonly", function(event) {
        //only allow digits (no negative, decimal, or slash)
        numbersOnly(event, /[^0-9]/);
    });

    $(document).on('keypress', ".dateEntry", function(event) {
        //only allow digits and slash
        numbersOnly(event, /[^0-9\/]/);
    });
})

function checkWeekRange() {
    var val = Number(this.value);
    if (val > 52 || val < 1) {
        alert("Please enter a valid number of weeks to display between 1 and 52");
        if (val > 52) {
            $(this).val(52);
        } else {
            $(this).val(1);
        }
    }

    get_data();
}

function nextPage() {
    page = page + 1;
    if (page > 0) {
        $('.prevPage').attr('disabled', false).removeClass('disabled');
    }

    get_data();
}

function prevPage() {
    if (page == 0) {
        return; //No previous page
    }
    page = page - 1;
    if (page == 0) {
        $('.prevPage').attr('disabled', true).addClass('disabled');
    }
    get_data();
}

//debounce with 100ms delay to prevent double calling when clicking 
//refresh after entering a new weeks value, both of which call get_data
var get_data = debounce(get_data_actual, 100)

function get_data_actual(filter) {
    console.log("Called get data actual");
    $('.inner').append('<div class="loadingOverlay">');

    if (typeof(filter) == "undefined") {
        filter = $('#locView').val();
    }
    var numWeeks = $('#numWeeks').val();
    if (filter == -1) {
        filter = numWeeks;
    }

    var endDate = $('#endDate').val();

    var args = {
        'filter': filter,
        'page': page,
        'weeks': numWeeks,
        'endDate': endDate
    };

    $.getJSON('weeklysummary/data', args)
        .done(processData)
        .fail(function(a, b, c) {
            alert(`Error when loading data: ${c}
Please try again. If this problem persists, contact support.`);
            $('#plot').html('<div style="text-align:center"><h1 style="color:red;">ERROR Loading Data</h1><br>Please reload page to try again</div>')
        })
}

function show_legend() {
    window.open('static/img/legend_detailed.png');
}

function processData(data) {
    $('#dateFrom').text(data['from']);
    $('#dateTo').text(data['to']);
    var cont = $('<div class="outter">');
    var inner = $('<div class="inner">');

    var left_markers = $('<div class="leftMarkers">');
    inner.append(left_markers);
    var right_markers = $('<div class="rightMarkers">');
    inner.append(right_markers);

    var navCont = $('<div class="navArrows">');
    cont.append(navCont);

    navCont.append('<button class="prevPage">');
    navCont.append('<div class="filler">');
    navCont.append('<button class="nextPage">');

    var dateCont = $('<div class="dateList">');
    cont.append(dateCont);
    var results = data['results'];
    var locs = data['places'];
    var dates = data['weeks'];
    for (var i = 0; i < dates.length; i++) {
        var datediv = $('<div class="date">');
        datediv.text(dates[i]);
        dateCont.append(datediv);
        if (i < dates.length - 1) {
            left_markers.append("<div>");
            right_markers.append("<div>");
        }
    }

    for (var i = 0; i < locs.length; i++) {
        let loc = locs[i];
        const locData = results[loc];
        const weekData = locData['week_data'];
        var locDiv = $('<div class="location">');
        var titleDiv = $('<div class="locTitle">');
        
        //Capitalize the name
        const locName=loc.split(" ");
        for (let i = 0; i < locName.length; i++) {
            locName[i] = locName[i][0].toUpperCase() + locName[i].substr(1);
        }
        loc=locName.join(' ');
        titleDiv.text(loc + " (" + locData['week_count'] + ")");
        locDiv.append(titleDiv);
        inner.append(locDiv);

        for (var j = 0; j < weekData.length - 1; j++) {
            var wdata = weekData[j];
            var wDivWrapper = $('<div class="weekBlock">');
            var wDiv = $('<div class="weekDetail">');
            wDiv.attr('title', wdata['count']);
            wDivWrapper.append(wDiv);
            if (wdata['percentile'] > 50) {
                var textDiv = $('<div class="weekText">');
                textDiv.text(wdata['count']);
                wDiv.append(textDiv);
            }
            wDiv.addClass("percent" + wdata['percentile']);
            wDiv.css('background-color', `rgba(${wdata['color']})`);
            locDiv.append(wDivWrapper);
        }
    }

    cont.append(inner);

    $('#plot').html(cont);
    if (page == 0) {
        $('.prevPage').attr('disabled', true).addClass('disabled');
    }
}

function numbersOnly(event, filter) {
    if (typeof filter == 'undefined')
        filter = /[^0-9\/\-\.]/;

    var object = $(event.target);
    var keyPressed = (event.keyCode || event.which);
    var valid_key = true;
    if (typeof event.which == "number" && event.which < 32) //delete, tab, etc
        return true;

    var keyChar = String.fromCharCode(keyPressed);
    var curValue = object.val();

    //make sure the key pressed is valid for a number - that is, a digit from 0-9, a negative sign, or a decimal
    //we also allow / so we can use this same filter as a basis for a date filter.
    if (keyChar.match(filter)) {
        valid_key = false;
    }
    //if we got this far, then the character itself is nominally valid. Now let's make sure
    //the usage is valid. For this, we need the current value of the input.
    else if (curValue != null) //if we can't get the value, just stop here to prevent errors. May lead to bad input, but we'll live
    {
        //First, negatives can only go at the beginning.
        if (keyChar == "-" && curValue.length != 0) //already have other characters. Can't type a -
        {
            valid_key = false;
        }
        //check for valid decimal usage
        else if (keyChar == ".") {
            if (curValue.length == 0 || curValue == '-') //can not be the first character, or right after a leading negative. Lead with a zero.
            {
                object.val(object.val() + "0");
            } else if (curValue.indexOf(".") != -1) //can not have more than 1 decimal
                valid_key = false;
        }
    }

    if (!valid_key) {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
        return false;
    }
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};