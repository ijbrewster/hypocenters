// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monUI, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////
    var refreshTimer = {}, refreshNotificationTimer = {}, lastRefreshUnixtime = 0, REFRESH_FREQUENCY_MINUTES = 5;
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monUI.showLegend = false;
    monUI.showVolcanoes = false;
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function startNotificaitonTimer() {
        
        var secondsRemaining = Math.round(
                    (REFRESH_FREQUENCY_MINUTES * 60) - (((new Date()).getTime() - lastRefreshUnixtime) / 1000)
                );
        
        $('#refreshDisplay').html('Refreshing data in ' + secondsRemaining + ' seconds.<br/>');
        clearTimeout(refreshNotificationTimer);
        refreshNotificationTimer = setTimeout(function() { startNotificaitonTimer(); }, 1000);
    }
    
    function startRefreshTimer() {

        if (!monMain.isDesktop) { return; }
        lastRefreshUnixtime = (new Date()).getTime();
        startNotificaitonTimer();
        clearTimeout(refreshTimer);
        refreshTimer = setTimeout(function() {
            monQuakeData.updateQuakeData();
            startRefreshTimer();
        }, 1000 * 60 * REFRESH_FREQUENCY_MINUTES);
    }

    function stopRefreshTimer() {

        clearTimeout(refreshTimer);
        clearTimeout(refreshNotificationTimer);
        $('#refreshDisplay').empty();
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monUI.toggleDepthUnits = function() {

        persistentOptions.put('kmOrMi', (persistentOptions.get('kmOrMi') === 'mi' ? 'km' : 'mi'));
        if (monMain.isDesktop) {
            monQuakeList.renderHtmlTable();
        } else {
            monQuakeList.renderRowsAndCols();
        }
        monUI.setUIForDepthUnits();
        monQuakeMap.renderQuakes();
        monPlotChart.renderAll();
        monLegend.renderQuakeInfo();
    };
    
    monUI.setUIForDepthUnits = function() {

        if (persistentOptions.get('kmOrMi') === 'km') {
            $('.vscmm-btn-settings[data-depth-setting="km"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-depth-setting="mi"]').removeClass('btn-primary').addClass('btn-default');
        } else {
            $('.vscmm-btn-settings[data-depth-setting="mi"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-depth-setting="km"]').removeClass('btn-primary').addClass('btn-default');
        }
    };
     
    monUI.toggleQuakeColor = function() {

        if (persistentOptions.get('quakeColorChoice') === 'depth') {
            persistentOptions.put('quakeColorChoice', 'time');
        } else {
            persistentOptions.put('quakeColorChoice', 'depth');
        }
        monUI.setUIForQuakeColor();
        monQuakeMap.renderQuakes();
        monPlotChart.renderAll();
        monLegend.renderQuakeInfo();
    };
    
    monUI.setUIForQuakeColor = function() {

        if (persistentOptions.get('quakeColorChoice') === 'depth') {
            $('.vscmm-btn-settings[data-color-setting="depth"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-color-setting="time"]').removeClass('btn-primary').addClass('btn-default');
        } else {
            $('.vscmm-btn-settings[data-color-setting="time"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-color-setting="depth"]').removeClass('btn-primary').addClass('btn-default');
        }
    };
            
    monUI.toggleTimeUnits = function() {

        persistentOptions.put('utcOrLocal', (persistentOptions.get('utcOrLocal') === 'UTC' ? 'local' : 'UTC'));
        if (monMain.isDesktop) {
            monQuakeList.renderHtmlTable();
        } else {
            monQuakeList.renderRowsAndCols();
        }
        monUI.setUIForTimeUnits();
        monQuakeMap.renderQuakes();
        monPlotChart.renderAll();
        monLegend.renderQuakeInfo();
    };
    
    monUI.setUIForTimeUnits = function() {

        if (persistentOptions.get('utcOrLocal') === 'UTC') {
            $('.vscmm-btn-settings[data-time-setting="utc"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-time-setting="local"]').removeClass('btn-primary').addClass('btn-default');
        } else {
            $('.vscmm-btn-settings[data-time-setting="local"]').addClass('btn-primary').removeClass('btn-default');
            $('.vscmm-btn-settings[data-time-setting="utc"]').removeClass('btn-primary').addClass('btn-default');
        }
    };
    
    monUI.toggleLegend = function(onOrOff) {

        if (onOrOff === '') { onOrOff = monUI.showLegend ? 'off' : 'on'; } // Reverse stored selection.

        persistentOptions.put('legendSetting', onOrOff);
        
        monUI.showLegend = (onOrOff === 'on');
        if (monUI.showLegend) {
            $('#vscmmLegend').show();
            $('#vscmmLegendShowButton').hide();
            $('.vscmm-btn-settings[data-legend-setting="off"]').removeClass('btn-primary');
            $('.vscmm-btn-settings[data-legend-setting="on"]').addClass('btn-primary');
        } else {
            $('#vscmmLegend').hide();
            $('#vscmmLegendShowButton').show();
            $('.vscmm-btn-settings[data-legend-setting="off"]').addClass('btn-primary');
            $('.vscmm-btn-settings[data-legend-setting="on"]').removeClass('btn-primary');
        }
    };

    monUI.toggleVolcanoes = function(onOrOff) {

        if (onOrOff === '' || typeof(onOrOff)=='undefined') { 
            onOrOff = monUI.showVolcanoes ? 'off' : 'on';  // Reverse stored selection.
        }

        persistentOptions.put('volcanoSetting', onOrOff);

        const volc_layers=[
            monVolcMap.unassignedLayer,
            monVolcMap.greenLayer,
            monVolcMap.yellowLayer,
            monVolcMap.orangeLayer,
            monVolcMap.redLayer
        ]
        
        monUI.showVolcanoes = (onOrOff === 'on');

        for(const layer of volc_layers){
            if(typeof(layer)==='undefined'){
                // Volc layers haven't been defined yet, bail out
                return; 
            }

            if(monUI.showVolcanoes){
                layer.show();
            }else{
                layer.hide();
            }
        }

        monVolcMap.showHideVolcNamesBasedOnZoom(monUiOptions.mapZoom);

        if (monUI.showVolcanoes) {
            $('.vscmm-btn-settings[data-volcano-setting="off"]').removeClass('btn-primary');
            $('.vscmm-btn-settings[data-volcano-setting="on"]').addClass('btn-primary');
        } else {
            $('.vscmm-btn-settings[data-volcano-setting="off"]').addClass('btn-primary');
            $('.vscmm-btn-settings[data-volcano-setting="on"]').removeClass('btn-primary');
        }
    };
    
    monUI.toggleAutoData = function(onOrOff) {

        if (onOrOff === '') { onOrOff = monUI.automaticData ? 'off' : 'on'; } // Reverse stored selection.

        persistentOptions.put('automaticData', onOrOff);
        
        monUI.automaticData = (onOrOff === 'on');
        if (monUI.automaticData) {
            startRefreshTimer();
            $('.vscmm-btn-settings[data-automatic-setting="off"]').removeClass('btn-primary');
            $('.vscmm-btn-settings[data-automatic-setting="on"]').addClass('btn-primary');
        } else {
            stopRefreshTimer();
            $('.vscmm-btn-settings[data-automatic-setting="off"]').addClass('btn-primary');
            $('.vscmm-btn-settings[data-automatic-setting="on"]').removeClass('btn-primary');
        }
    };
    
}(window.monUI = window.monUI || {}, jQuery));
