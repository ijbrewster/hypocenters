// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function(monQuakeUI, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var unfilteredTotal = 0,
        visibleOnMapTotal = 0,
        ageSelection = '',
        magToggle = {},
        depthToggle = { '0-5': false, '5-10': false, '10-15': false, '15-20': false, '20+': false },
        filterDists={};

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function unHide() {
        persistentOptions.put('isHideAllQuakes', false);
    }

    function applyFilters() {

        unfilteredTotal = 0;
        visibleOnMapTotal = 0;

        monQuakeData.magBuckets['0'] = 0;
        monQuakeData.magBuckets['1'] = 0;
        monQuakeData.magBuckets['2'] = 0;
        monQuakeData.magBuckets['3'] = 0;
        monQuakeData.magBuckets['4'] = 0;

        monQuakeData.depthBuckets['0-5'] = 0;
        monQuakeData.depthBuckets['5-10'] = 0;
        monQuakeData.depthBuckets['10-15'] = 0;
        monQuakeData.depthBuckets['15-20'] = 0;
        monQuakeData.depthBuckets['20+'] = 0;

        if (persistentOptions.get('isHideAllQuakes')) {
            for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) { // Set filter if hide all is selected.
                monQuakeData.quakeRecs[idx].isFiltered = true;
                if (monQuakeData.quakeRecs[idx].isVisibleOnMap) {
                    visibleOnMapTotal++;
                }
            }
        } else {
            let selectedFilterDist=$('#filterDist').val();
            const showAuto=$('#filterAuto').val()==="1" // 0 or 1
            for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
                var quakeRec = monQuakeData.quakeRecs[idx];

                if(!quakeRec.isFiltered){
                    let dist=filterDists[quakeRec.eventId];

                    if(typeof(dist)!=='undefined' && dist>selectedFilterDist){
                        quakeRec.isFiltered=true;
                    }
                }

                //see if is an auto location, and if we want to show those
                if(!quakeRec.isFiltered){
                    if(quakeRec.auto && !showAuto){
                        quakeRec.isFiltered=true;
                    }
                }

                //If we aren't filtered, see if our age matches the selected age, and if not filter
                if (!quakeRec.isFiltered) {
                    var ageSelected = false;
                    if (ageSelection === 'last20' && quakeRec.isLast20) { ageSelected = true; } else
                    if (ageSelection === 'day' && quakeRec.ageDays <= 0) { ageSelected = true; } else
                    if (ageSelection === 'week' && quakeRec.ageDays <= 6) { ageSelected = true; } else
                    if (ageSelection === 'month' && quakeRec.ageDays <= 31) { ageSelected = true; } else
                    if (ageSelection === 'year' && quakeRec.ageDays<=365) {ageSelected = true; } else
                    if (ageSelection === 'custom') {
                        var custFrom = new Date($('#customAgeFrom').val())
                        var custTo = new Date($('#customAgeTo').val())
                        if (quakeRec.date >= custFrom && quakeRec.date <= custTo) { ageSelected = true; }
                    } else
                    if (ageSelection === 'any') { ageSelected = true; }
                    quakeRec.isFiltered = !ageSelected;
                }

                //need another if because this might have changed in the previous block
                if (!quakeRec.isFiltered) {
                    //update proper magnitude bucket if quake is not filtered
                    if(quakeRec.isVisibleOnMap){
                        var destBucket = Math.floor(quakeRec.mag);
                        if (destBucket <= 0) { monQuakeData.magBuckets['0'] += 1; } else
                        if (destBucket == 1) { monQuakeData.magBuckets['1'] += 1; } else
                        if (destBucket == 2) { monQuakeData.magBuckets['2'] += 1; } else
                        if (destBucket == 3) { monQuakeData.magBuckets['3'] += 1; } else { monQuakeData.magBuckets['4'] += 1; }
                    }

                    var magSelected = false;
                    for (var key in magToggle) {
                        if (magToggle[key] && key === '0' && quakeRec.mag < 1) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '1' && quakeRec.mag >= 1 && quakeRec.mag < 2) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '2' && quakeRec.mag >= 2 && quakeRec.mag < 3) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '3' && quakeRec.mag >= 3 && quakeRec.mag < 4) {
                            magSelected = true;
                        }
                        if (magToggle[key] && key === '4' && quakeRec.mag >= 4) {
                            magSelected = true;
                        }
                    }
                    quakeRec.isFiltered = !magSelected;
                }

                if (!quakeRec.isFiltered) {
                    if(quakeRec.isVisibleOnMap){
                        if (quakeRec.depthKM < 5) {
                            monQuakeData.depthBuckets['0-5'] += 1;
                        } else if (quakeRec.depthKM < 10) {
                            monQuakeData.depthBuckets['5-10'] += 1;
                        } else if (quakeRec.depthKM < 15) {
                            monQuakeData.depthBuckets['10-15'] += 1;
                        } else if (quakeRec.depthKM < 20) {
                            monQuakeData.depthBuckets['15-20'] += 1;
                        } else {
                            monQuakeData.depthBuckets['20+'] += 1;
                        }
                    }

                    var depthSelected = false;
                    for (var key in depthToggle) {
                        if (depthToggle[key] && key === '0-5' && quakeRec.depthKM < 5) {
                            depthSelected = true;
                        }
                        if (depthToggle[key] && key === '5-10' && quakeRec.depthKM >= 5 && quakeRec.depthKM < 10) {
                            depthSelected = true;
                        }
                        if (depthToggle[key] && key === '10-15' && quakeRec.depthKM >= 10 && quakeRec.depthKM < 15) {
                            depthSelected = true;
                        }
                        if (depthToggle[key] && key === '15-20' && quakeRec.depthKM >= 15 && quakeRec.depthKM < 20) {
                            depthSelected = true;
                        }
                        if (depthToggle[key] && key === '20+' && quakeRec.depthKM >= 20) {
                            depthSelected = true;
                        }
                    }
                    quakeRec.isFiltered = !depthSelected;
                }

                if (!quakeRec.isVisibleOnMap) { continue; }

                visibleOnMapTotal++;

                unfilteredTotal += (quakeRec.isFiltered ? 0 : 1);
            }
        }

        var visibleHtml = unfilteredTotal + ' Events Visible (of ' + visibleOnMapTotal + ')';
        if (unfilteredTotal === 1) {
            visibleHtml = unfilteredTotal + ' Event Visible (of ' + visibleOnMapTotal + ')';
        }

        $('#vscmmQuakeTable').find('a').html(visibleHtml);

        if (monMain.isDesktop) {
            if (unfilteredTotal < visibleOnMapTotal) { $('.vscmm-btn-quake-show').fadeIn(); }
            if (unfilteredTotal === visibleOnMapTotal) { $('.vscmm-btn-quake-show').fadeOut(); }
            if (unfilteredTotal > 0) { $('.vscmm-btn-quake-hide').fadeIn(); }
            if (unfilteredTotal === 0) { $('.vscmm-btn-quake-hide').fadeOut(); }
        } else {
            if (unfilteredTotal < visibleOnMapTotal) { $('.vscmm-btn-quake-show').show(); }
            if (unfilteredTotal === visibleOnMapTotal) { $('.vscmm-btn-quake-show').hide(); }
            if (unfilteredTotal > 0) { $('.vscmm-btn-quake-hide').show(); }
            if (unfilteredTotal === 0) { $('.vscmm-btn-quake-hide').hide(); }
        }
    };

    function hideAllUI() {

        persistentOptions.put('isHideAllQuakes', true);

        ageSelection = '';
        persistentOptions.put('quakeAgeSelection', '');
        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');

        for (var idx = 0; idx < 7; idx++) { magToggle[idx] = false; }
        persistentOptions.put('quakeMagToggle', magToggle);
        $('.btn-mag').removeClass('btn-primary');
        $('.btn-mag').addClass('btn-default');

        for (var key in depthToggle) { depthToggle[key] = false; }
        persistentOptions.put('quakeDepthToggle', depthToggle);
        $('.btn-depth').removeClass('btn-primary');
        $('.btn-depth').addClass('btn-default');
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeUI.showLess = function(evt) {

        $('#vscmmEmptyModal').modal('hide');
        $('#vscmmMapHover').hide();

        var topOffset = $('#vscmmMapContainer').position().top + evt.screenPoint.y + 12, //top_offset;
            leftOffset = $('#vscmmMapContainer').position().left + evt.screenPoint.x + 12; // Left offset

        if (!monMain.isDesktop) {
            topOffset = $('#vscmmMapContainerMobile').position().top + evt.screenPoint.y + 2, //top_offset;
                leftOffset = $('#vscmmMapContainerMobile').position().left + evt.screenPoint.x + 2; // Left offset
        }

        monQuakeMap.activateQuake(evt.graphic.attributes.eventId);
        $('#vscmmMapMobilePopup').css({ 'top': topOffset + 'px', 'left': leftOffset + 'px' });
        $('#vscmmMapMobilePopup').html(evt.graphic.attributes.popupHtml);
        $('#vscmmMapMobilePopup').show();

        if (!monMain.isDesktop) {
            if ((leftOffset + $('#vscmmMapMobilePopup').outerWidth()) > document.documentElement.clientWidth) {

                var newLeft = leftOffset - 214;
                if (newLeft < 0) { newLeft = 10; }

                $('#vscmmMapMobilePopup').css({
                    'left': newLeft + 'px',
                    'min-width': '214px'
                });
            }
        }
    };

    monQuakeUI.getWaveformLink = function(net, id) {
        if (net.toUpperCase() == "HV") {
            return "<a href ='http://hvo.wr.usgs.gov/seismic/tux/waveforms/hv" + id + ".html' target=\"_blank\">";
        } else if (net.toUpperCase() == "AV") {
            return "<a href ='http://www.avo.alaska.edu/volcweb/earthquakes/events/" + id + ".gif' target=\"_blank\">";
        } else {
            return "<a>";
        }
    }

    monQuakeUI.processDistances = function(distances) {
        var distMI = []
        var distKM = []
        var bearings = []
        var directions = []

        for (var idx in distances) {
            var item = distances[idx];
            distMI.push((item['dist_km'] * 0.621).toFixed(1))
            distKM.push(Number(item['dist_km']).toFixed(1))
            bearings.push(item['bearing'])
            directions.push(item['direction'])
        }

        // One or all of these may be undefined
        try {
            var distance1 = distKM[0] + " km (" + distMI[0] + " mi) " + directions[0] + " of " + distances[0]['name'] + " (" + Math.round(bearings[0]) + " deg)<br>";
        } catch {
            var distance1 = 'Unknown<br>';
        }

        try {
            var distance2 = distKM[1] + " km (" + distMI[1] + " mi) " + directions[1] + " of " + distances[1]['name'] + " (" + Math.round(bearings[1]) + " deg)<br>";
        } catch {
            var distance2 = "Unknown<br>";
        }

        try {
            var distance3 = distKM[2] + " km (" + distMI[2] + " mi) " + directions[2] + " of " + distances[2]['name'] + " (" + Math.round(bearings[2]) + " deg)<br>";
        } catch {
            var distance3 = "Unknown<br>"
        }

        return distance1 + distance2 + distance3
    }

    monQuakeUI.showDetail = function(eventId) {
        const feelLink=`https://earthquake.usgs.gov/earthquakes/eventpage/${eventId}#tellus`
        const detailLink=`https://earthquake.usgs.gov/earthquakes/eventpage/${eventId}`
        const eventData={
            'ID': eventId,
            'Network': 'AV',
        }

        const eventRecord=monQuakeData.quakeRecs.find(x=>x.eventId==eventId)
        let needPull=[];
        const keys=['lat','long','depthKM','mag','ageDays','num-phases-used','azimuthal-gap',
                    'vertical-error','horizontal-error','quakeDateString','quakeUTCString','rms']

        for(let key of keys){
            let val=eventRecord[key];
            if(typeof(val)=='undefined'){
                needPull.push(key);
                continue;
            }

            eventData[key]=val;
        }

        //handle some calculated values
        eventData['depthMI']=monUtils.round(monUtils.kmToMi(eventData.depthKM), 1)
        eventData['Days']=monUtils.round(eventRecord.ageDays, 1)

        //And links
        $('#quakeDetailLink').attr('href',detailLink)
        $('#quakeDetailDidYouFeel').attr('href',feelLink)

        //see if we need to get distances
        if(typeof(eventRecord.distances)!=='undefined'){
            eventData['Distance']=monQuakeUI.processDistances(eventRecord.distances)
        } else{
            //do an asynchronous pull/populate.
            $.getJSON('getDistances',{'lat':eventRecord.lat,'lon':eventRecord.long})
            .done(function(result){
                const distanceHTML=monQuakeUI.processDistances(result)
                eventRecord.distances=result
                $('#quakeDetailDistance').html(distanceHTML)
            })
        }

        eventData['autoLocation']=eventRecord.auto?"(Auto-Location, Unreviewed)":''

        //Populate the display fields
        for(const key in eventData){
            let id=`#quakeDetail${key}`
            $(id).html(eventData[key]);
        }

        $('#quakeDetail').modal('show');

        if(needPull.length==0){
            return
        }

        monQuakeData.getDetail(eventId, function(detailRec) {
            for(let key of needPull){
                eventData[key]=detailRec[key]
            }

            //Populate the display fields
            for(const key in eventData){
                let id=`#quakeDetail${key}`
                $(id).html(eventData[key]);
            }
        });

    };

    monQuakeUI.showAll = function() {

        monQuakeUI.reset();
        persistentOptions.put('quakeAgeSelection', 'any');
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.hideAll = function() {

        hideAllUI();
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.setAge = function() {
        var age = $(this).data('age');

        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            for (var key in depthToggle) { monQuakeUI.toggleDepth(key); }
            for (var key in magToggle) { monQuakeUI.toggleMag(key); }
        }
        $('input.age').removeClass('error');
        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');
        ageSelection = age;
        $('.btn-age[data-age="' + age + '"]').removeClass('btn-default');
        $('.btn-age[data-age="' + age + '"]').addClass('btn-primary');

        persistentOptions.put('lastQuakeAgeSelection', persistentOptions.get('quakeAgeSelection'))
        persistentOptions.put('quakeAgeSelection', age);
        monMain.mapExtentChangeFiredOrFiltersChanged();
        persistentOptions.put('lastQuakeAgeSelection', age);
    };

    monQuakeUI.toggleMag = function(mag) {

        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            if (ageSelection === '') { monQuakeUI.setAge('any'); }
            for (var key in depthToggle) { monQuakeUI.toggleDepth(key); }
        }

        if (magToggle[mag]) {
            magToggle[mag] = false;
            $('.btn-mag[data-mag="' + mag + '"]').removeClass('btn-primary');
            $('.btn-mag[data-mag="' + mag + '"]').addClass('btn-default');
        } else {
            magToggle[mag] = true;
            $('.btn-mag[data-mag="' + mag + '"]').removeClass('btn-default');
            $('.btn-mag[data-mag="' + mag + '"]').addClass('btn-primary');
        }
        persistentOptions.put('quakeMagToggle', magToggle);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.toggleDepth = function(depthSelection) {

        if (persistentOptions.get('isHideAllQuakes')) {
            unHide();
            if (ageSelection === '') { monQuakeUI.setAge('any'); }
            for (var key in magToggle) { monQuakeUI.toggleMag(key); }
        }

        if (depthToggle[depthSelection]) {
            depthToggle[depthSelection] = false;
            $('.btn-depth[data-depth="' + depthSelection + '"]').removeClass('btn-primary');
            $('.btn-depth[data-depth="' + depthSelection + '"]').addClass('btn-default');
        } else {
            depthToggle[depthSelection] = true;
            $('.btn-depth[data-depth="' + depthSelection + '"]').removeClass('btn-default');
            $('.btn-depth[data-depth="' + depthSelection + '"]').addClass('btn-primary');
        }
        persistentOptions.put('quakeDepthToggle', depthToggle);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.reset = function() {

        unHide();
        persistentOptions.put('quakeAgeSelection', 'week');
        persistentOptions.put('quakeMagToggle', false);
        persistentOptions.put('quakeDepthToggle', false);
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeUI.resetOnly = function() {

        unHide();
        persistentOptions.put('quakeAgeSelection', 'week');
        persistentOptions.put('quakeMagToggle', false);
        persistentOptions.put('quakeDepthToggle', false);
    };

    let distanceDelayTimer=null;
    monQuakeUI.getDistancesDelay = function(){
        // "Debounce" function to allow entry of a multi-digit number, or deletion
        // of an existing value and entry of a new value without triggering calculation
        // during data entry
        if(distanceDelayTimer!==null){
            clearTimeout(distanceDelayTimer);
        }

        distanceDelayTimer=setTimeout(monQuakeUI.getDistances,700);
    }

    monQuakeUI.getDistances = function(){
        distanceDelayTimer=null;
        filterDists={}

        const origin=$('#filterDistOrigin').val();
        if(origin!=="[]"){
            $('#vscmmLoading .loading-message').html("Calculating quake distances...");
            $('#vscmmLoading').show(); //will be hidden by other processes later

            let originLat,originLon;
            [originLat,originLon]=JSON.parse(origin);
            for(const rec of monQuakeData.quakeRecs){
                let quakeLat=rec.lat;
                let quakeLon=rec.long;
                let dist=monUtils.distanceM(originLat,originLon,quakeLat,quakeLon); //in meters
                filterDists[rec.eventId]=dist/1000; //km
            }
        }

        applyFilters();
        monMain.mapExtentChangeFiredOrFiltersChanged();
    }

    monQuakeUI.refresh = function() {

        $('.btn-age').removeClass('btn-primary');
        $('.btn-age').addClass('btn-default');
        ageSelection = persistentOptions.get('quakeAgeSelection');

        $(`.btn-age[data-age="${ageSelection}"]`).removeClass('btn-default');
        $(`.btn-age[data-age="${ageSelection}"]`).addClass('btn-primary');

        if (persistentOptions.get('quakeMagToggle')) {
            magToggle = persistentOptions.get('quakeMagToggle');
        } else {
            for (var idx = 0; idx < 7; idx++) { magToggle[idx] = true; }
            persistentOptions.put('quakeMagToggle', magToggle);
        }
        for (var key in magToggle) {
            if (magToggle[key]) {
                $('.btn-mag[data-mag="' + key + '"]').removeClass('btn-default');
                $('.btn-mag[data-mag="' + key + '"]').addClass('btn-primary');
            }
        }

        if (persistentOptions.get('quakeDepthToggle')) {
            depthToggle = persistentOptions.get('quakeDepthToggle');
        } else {
            for (var key in depthToggle) { depthToggle[key] = true; }
            persistentOptions.put('quakeDepthToggle', depthToggle);
        }
        for (var key in depthToggle) {
            if (depthToggle[key]) {
                $('.btn-depth[data-depth="' + key + '"]').removeClass('btn-default');
                $('.btn-depth[data-depth="' + key + '"]').addClass('btn-primary');
            }
        }
        applyFilters();

        for (var key in monQuakeData.magBuckets) {
            $('.btn-mag[data-mag="' + key + '"] > .badge').html(monQuakeData.magBuckets[key]);
        }

        for (var key in monQuakeData.depthBuckets) {
            $('.btn-depth[data-depth="' + key + '"] > .badge').html(monQuakeData.depthBuckets[key]);
        }

        for (var key in monQuakeData.ageBuckets) {
            $('.btn-age[data-age="' + key + '"] > .badge').html(monQuakeData.ageBuckets[key]);
        }

        if (ageSelection == 'custom') {
            $('#customAge').slideDown();
        } else {
            $('#customAge').slideUp();
        }

        $('#vscmmLast20Label').html('Last ' + monQuakeData.ageBuckets['last20'] + " Events");

        if (persistentOptions.get('isHideAllQuakes')) {
            hideAllUI();
            applyFilters();
        }
    };

}(window.monQuakeUI = window.monQuakeUI || {}, jQuery));
