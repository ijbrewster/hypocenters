$(document).ready(function(){
    $(document).on('click','button.btn-age', monQuakeUI.setAge)
    $(document).on('change','input.age',monQuakeUI.setAge)
    $(document).on('hidden.bs.tab',"a[href='#random-cat']",update_cat);
    $(document).on('hidden.bs.modal','#vscmmPlotModal',update_cat);
    
    $(document).on('change','#magEnergy',swapMagEnergy)
    $(document).on('click','#refresh',updateQuakeData)
    $(document).on('click','#NextVolcano',function(){switchVolcano(0)})
    $(document).on('click','#PrevVolcano',function(){switchVolcano(-1)})
    $(document).on('click','#downloadGraphData', downloadGraphData);
    $(document).on('click','#downloadGraphEvents', downloadGraphEvents);
    $(document).on('click','#downloadGraphPDF', downloadGraphPDF);

    $(document).on('input', '#filterDist', monQuakeUI.getDistancesDelay);
    $(document).on('change','#filterDistOrigin',monQuakeUI.getDistances);
    $(document).on('change','#filterAuto',monMain.mapExtentChangeFiredOrFiltersChanged);
})

var cat_apis=[
    'https://cataas.com/cat?',
    'https://api.thecatapi.com/v1/images/search?format=src&'
]
function update_cat(){
    var random=new Date().getTime();
    var urlIdx=Math.floor(Math.random()*2);
    var url=cat_apis[urlIdx];
    $('#catImagePlotArea').attr('src',url+"_="+random);
}
function switchVolcano(step){
    var stepVolcano=$('#volcanoSelect option:selected').prop('selected',false)
    if (step>=0)
	stepVolcano=stepVolcano.nextAll('option:eq('+step+')')
    else
	stepVolcano=stepVolcano.prevAll('option:eq('+((step*-1)-1)+')')
	
    stepVolcano.prop('selected', 'selected')
    $('#volcanoSelect').trigger('change');
}

function swapMagEnergy(){
	var trace=this.value;
	var hide=trace==0?1:0;
	
	var plotDiv=$('#cumMagPlotArea')[0]
	var plots=plotDiv.data;
	plots[trace].visible=true;
	plots[hide].visible=false;
	Plotly.redraw(plotDiv);
}

function updateQuakeData(){
    monQuakeData.updateQuakeData();
}

function selectVolcanoChanged(){
    //"this" is the dropdown menu
    var loc=$(this).find('option:selected').data('loc')

    if(typeof(loc)=='undefined'){
        return; //no location associated with this volcano yet.
    }
    
    var center=new esri.geometry.Point(loc[1],loc[0])
    monMap.setCenterAndZoom(center,loc[2])
}

function downloadGraphPDF(){
    const plotDiv=$('.js-plotly-plot:visible').get(0)
    const plotData=plotDiv.data;
    const plotLayout=plotDiv.layout;
    const args={
	'data':plotData,
	'layout':plotLayout
    }
    
    //Have to do this as a form so we can do a post
    const form=$('<form method="post" style="display:none" action="getPlotPDF">')
    const data=$('<input type=text name="data">').val(JSON.stringify(plotData))
    const layout=$('<input type=text name="layout">').val(JSON.stringify(plotLayout))
    form.append(data)
    form.append(layout)
    $("body").append(form)
    form.submit();
    form.remove();
}

function downloadGraphData(){
    const plotData=$('.js-plotly-plot:visible')[0].data;
    for(const data of plotData){
        const visible=data.visible;
        if(typeof(visible)=='undefined' || visible){
            //Create a form to post this request
            const form=$('<form method="post" style="display:none" action="getPlotCSV">')
            const xdata=$('<input type=text name=x>').val(JSON.stringify(data.x))
            const ydata=$('<input type=text name=y>').val(JSON.stringify(data.y));
            form.append(xdata);
            form.append(ydata);

            if (typeof(data.records)!='undefined'){
                const records=$('<input type=text name="records">').val(JSON.stringify(data.records));
                form.append(records);
            }
         
            $("body").append(form);
            form.submit();
            form.remove();
            break
        }
    }
}

function downloadGraphEvents(){
    const events=monPlotChart.quakesInPolygon
    monQuakeData.downloadQuakeData(events);
}