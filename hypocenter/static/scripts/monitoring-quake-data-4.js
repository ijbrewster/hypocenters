// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function(monQuakeData, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var urlParamsLastTry = '',
        detailRecs = {},
        dataMinLong = 0.0,
        dataMaxLong = 0.0,
        dataMinLat = 0.0,
        dataMaxLat = 0.0,
        thresholdStatus = '';

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeData.quakeRecs = [];
    monQuakeData.quakeIDs = [];
    monQuakeData.minMag = 0.0;
    monQuakeData.maxMag = 0.0;
    monQuakeData.minDepthKM = 0.0;
    monQuakeData.maxDepthKM = 0.0;
    monQuakeData.ageBuckets = {};
    monQuakeData.magBuckets = {};
    monQuakeData.depthBuckets = {};

    // FUNCTION QUEUE:
    // https://stackoverflow.com/questions/899102/how-do-i-store-javascript-functions-in-a-queue-for-them-to-be-executed-eventuall
    monQuakeData.functionQueue = [];

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function processDetailAfterPull(detailRec) {

        //we want to look at the AV solution, if available. Otherwise just use the "prefered" solution.
        var av_origin = detailRec.properties.products.origin.find(function(element) { return element['source'] == 'av' })
        if (typeof(av_origin) !== 'undefined') {
            detailRec.lat = av_origin.properties.latitude;
            detailRec.long = av_origin.properties.longitude;
            detailRec.mag = Math.round(parseFloat(av_origin.properties.magnitude) * 100) / 100;
            detailRec.depthKM = av_origin.properties.depth;
        } else {
            detailRec.lat = detailRec.geometry.coordinates[1];
            detailRec.long = detailRec.geometry.coordinates[0];
            detailRec.mag = Math.round(parseFloat(detailRec.properties.mag) * 100) / 100;
            detailRec.depthKM = detailRec.geometry.coordinates[2];
        }

        detailRec.quakeDate = new Date(detailRec.properties.time);
        detailRec.localTime = new Date(detailRec.properties.localTime);
        detailRec.quakeUTCString = monUtils.dateToUTCString(detailRec.quakeDate);
        detailRec.quakeDateString = monUtils.dateToString(detailRec.quakeDate);
        var refDate = new Date();
        refDate.setUTCHours(0, 0, 0, 0);
        detailRec.ageDays = monUtils.dateDiffHH(refDate, detailRec.quakeDate) / 24;
        detailRec.realAgeDays = monUtils.dateDiffHH(new Date(), detailRec.quakeDate) / 24;

        //get distance information if needed
        if (typeof(detailRec.distances) === "undefined") {
            $.ajax({
                url: "getDistances?lat=" + detailRec.lat + "&lon=" + detailRec.long,
                async: false,
                dataType: "json"
            }).done(function(result) {
                detailRec.distances = result;
            });
        }

        return detailRec;
    }

    function refreshQuakeData() {

        //age
        monQuakeData.ageBuckets['day'] = 0;
        monQuakeData.ageBuckets['week'] = 0;
        monQuakeData.ageBuckets['month'] = 0;
        monQuakeData.ageBuckets['year']=0;
        monQuakeData.ageBuckets['any'] = 0;
        monQuakeData.ageBuckets['custom'] = 0;
        monQuakeData.ageBuckets['last20'] = 0;

        var countTo20 = 0;

        // Sort by unixDate desc.
        monQuakeData.quakeRecs.sort(sortQuakesNewest);

        //get the custom date ranges
        var custFrom = new Date($('#customAgeFrom').val())
        var custTo = new Date($('#customAgeTo').val())

        //check for valid from date
        if (isNaN(custFrom)) { $('#customAgeFrom').addClass('error'); } else { $('#customAgeFrom').removeClass('error'); }

        //check for valid to date
        if (isNaN(custTo)) { $('#customAgeTo').addClass('error'); } else { $('#customAgeTo').removeClass('error'); }

        //show error if *either* is invalid, hide if *both* are valid
        if (isNaN(custFrom) || isNaN(custTo)) { $('#dateError').slideDown(); } else { $('#dateError').slideUp(); }

        for (var idx = 0, totalQuakes = monQuakeData.quakeRecs.length; idx < totalQuakes; idx++) {

            var quakeRec = monQuakeData.quakeRecs[idx];

            quakeRec.isVisibleOnMap = monMap.isVisibleOnMap(quakeRec.lat, quakeRec.long);
            quakeRec.isFiltered = false;
            quakeRec.isLast20 = (quakeRec.isVisibleOnMap ? countTo20 < 20 : false);
            if (quakeRec.isVisibleOnMap) { countTo20++; }
            if (!quakeRec.isVisibleOnMap) { continue; }


            // Set up monQuakeData.ageBuckets
            if (quakeRec.isLast20) { monQuakeData.ageBuckets['last20']++; }
            if (quakeRec.ageDays <= 0) { monQuakeData.ageBuckets['day']++; }
            if (quakeRec.ageDays <= 6) { monQuakeData.ageBuckets['week']++; }
            if (quakeRec.ageDays <= 31) { monQuakeData.ageBuckets['month']++; }
            if (quakeRec.ageDays <= 365) { monQuakeData.ageBuckets['year']++; }
            if (quakeRec.date <= custTo && quakeRec.date >= custFrom) { monQuakeData.ageBuckets['custom']++; }

            monQuakeData.ageBuckets['any']++;

            if (quakeRec.mag < monQuakeData.minMag) { monQuakeData.minMag = quakeRec.mag; }
            if (quakeRec.mag > monQuakeData.maxMag) { monQuakeData.maxMag = quakeRec.mag; }

            if (quakeRec.depthKM < monQuakeData.minDepthKM) { monQuakeData.minDepthKM = quakeRec.depthKM; }
            if (quakeRec.depthKM > monQuakeData.maxDepthKM) { monQuakeData.maxDepthKM = quakeRec.depthKM; }
        }
    }

    function sortQuakesNewest(a, b) {

        if (parseFloat(a['unixTime']) > parseFloat(b['unixTime'])) { return -1; }
        if (parseFloat(a['unixTime']) < parseFloat(b['unixTime'])) { return 1; }
        return 0;
    }

    function getQuakeData(callback) {
        var age = persistentOptions.get("quakeAgeSelection");

        var now_day = new Date();
        now_day.setUTCHours(0, 0, 0, 0)

        let starttime=null;
        let endtime=null;
        if (age == 'custom') {
            try{
                let start_date = new Date($('#customAgeFrom').val());
                start_date.setUTCHours(0, 0, 0, 0);
                starttime = start_date.toISOString()

                let end_date = new Date($('#customAgeTo').val());
                end_date.setUTCHours(23, 59, 59, 999);
                endtime = end_date.toISOString();
            } catch{
                alert("Unable to parse custom start or end date.\n\nDates must be formatted as YYYY-MM-DD");
                if (typeof callback === 'function') {
                    callback();
               }
            }
        }


        $.getJSON('getEvents', {'startTime':starttime,
                                'endTime':endtime})
            .done(function(data) {
                for (var idx = 0; idx < data.length; idx++) {

                    let quakeRec = data[idx],
                    lat = quakeRec.lat,
                    long = quakeRec.long;

                    var eventId = quakeRec.eventId;

                    if (monQuakeData.quakeIDs.includes(eventId)) {
                        //We have already mapped this quake, don't do it again.
                        continue;
                    } else {
                        monQuakeData.quakeIDs.push(eventId)
                    }

                    quakeRec.date = new Date(quakeRec.unixTime);
                    quakeRec.url = quakeRec.url;
                    quakeRec.isVisibleOnMap = monMap.isVisibleOnMap(quakeRec.lat, quakeRec.long);
                    quakeRec.quakeUTCString = monUtils.dateToUTCString(quakeRec.date);
                    quakeRec.quakeDateString = monUtils.dateToString(quakeRec.date);
                    quakeRec.ageDays = monUtils.dateDiffHH(now_day, quakeRec.date) / 24;
                    quakeRec.realAgeDays = monUtils.dateDiffHH(new Date(), quakeRec.date) / 24;

                    monQuakeData.quakeRecs.push(quakeRec);
                }
            })
            .fail(function(data, arg) {
                console.error("Unable to fetch quake data");
                alert("Error retrieving quake data!");
                console.log(data.statusCode());
            })
            .always(function() {
                //always run the callback, even if we failed to get any data.
                if (typeof callback === 'function') {
                     callback();
                }

                // since we just loaded data (or at least tried to), the date range selector has not changed
                // so the lastQuakeAgeSelection should be the same as the current at this point.
                const ageSelection = persistentOptions.get('quakeAgeSelection')
                persistentOptions.put('lastQuakeAgeSelection',ageSelection);
            });
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monQuakeData.updateQuakeData = function() { // Refresh quake data without changing map extents.

        monUtils.preventAccordion = true;
        dataMinLong = 0.0;
        dataMaxLong = 0.0;
        dataMinLat = 0.0;
        dataMaxLat = 0.0;
        thresholdStatus = '';
        urlParamsLastTry = '';
        monQuakeData.quakeRecs = [];
        monQuakeData.quakeIDs = [];
        monQuakeData.minMag = 0.0;
        monQuakeData.maxMag = 0.0;
        monQuakeData.minDepthKM = 0.0;
        monQuakeData.maxDepthKM = 0.0;
        monQuakeData.ageBuckets = {};
        monQuakeData.magBuckets = {};
        monQuakeData.depthBuckets = {};
        monMain.mapExtentChangeFiredOrFiltersChanged();
    };

    monQuakeData.refreshBucketTotals = function() {

        refreshQuakeData();
    };

    monQuakeData.isDataAlreadyPulled = function(minLongIn, minLatIn, maxLongIn, maxLatIn) {

        var ageSelection = persistentOptions.get('quakeAgeSelection')
        var lastSelection = persistentOptions.get('lastQuakeAgeSelection')

        //if switching to/from custom, always pull data
        if (ageSelection == 'custom' || lastSelection == 'custom') {
            return false;
        }

        // No data pulled yet
        if (dataMinLong === 0.0 && dataMaxLong === 0.0 && dataMinLat === 0.0 && dataMaxLat === 0.0) {
            dataMinLong = minLongIn;
            dataMaxLong = maxLongIn;
            dataMinLat = minLatIn;
            dataMaxLat = maxLatIn;
            return false;
        }

        return true;
    };

    monQuakeData.getQuakeRec = function(eventId) {

        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
            if (monQuakeData.quakeRecs[idx].eventId === eventId) { return monQuakeData.quakeRecs[idx]; }
        }
        return {}; // Nothing found.
    };

    monQuakeData.getDetail = function(eventId, callback) {

        if (detailRecs[eventId]) { // Only pull if we don't already have detail data.
            callback(detailRecs[eventId]);
            return;
        }

        var detailUrlWithParams = monUiOptions.quakesUrl + '?eventid=' + eventId + '&format=geojson';
        $.getJSON(detailUrlWithParams, function(data) {
            var detailRec = processDetailAfterPull(data);
            detailRecs[eventId] = detailRec;
            callback(detailRec);
        });
    };

    monQuakeData.getQuakes = function(callback) {
        //see if we have gotten any data yet
        if(monQuakeData.quakeRecs.length>0){
            //If we have, see if we need to get new data
            var age = persistentOptions.get("quakeAgeSelection");
            var lastSelection = persistentOptions.get('lastQuakeAgeSelection')
            if (lastSelection != 'custom' && age != "custom") {
                if (typeof callback === 'function') {
                    callback();
                }
                return;
            }
        }


        monQuakeData.quakeRecs = [];
        monQuakeData.quakeIDs = [];
        monQuakeData.minMag = 0.0;
        monQuakeData.maxMag = 0.0;
        monQuakeData.minDepthKM = 0.0;
        monQuakeData.maxDepthKM = 0.0;
        monQuakeData.ageBuckets = {};

        $('#vscmmLoading').show();
        $('#vscmmLoading .loading-message').html(
            'Pulling Earthquake data from external data source and processing data.'
        );

        getQuakeData(function() {
            refreshQuakeData();
            while (monQuakeData.functionQueue.length > 0) {
                (monQuakeData.functionQueue.shift())();
            }
            if (typeof callback === 'function') {
                callback();
            }
            //}
        });
    };

    monQuakeData.downloadQuakeData = function(selectedEvents) {
        //compile a list of visible earthquakes to send to the server
        //this has to happen server side in order to download a file. Sigh.
        if(typeof(selectedEvents)==='undefined'){
            selectedEvents=monQuakeData.quakeRecs;
        }
        
        let csvContent="Event ID,Date/Time UTC,Depth km,Magnitude,Latitude,Longitude,Distance From Closest Volcano\n"
        for (const quakeRec of selectedEvents) {
            if (quakeRec.isFiltered || !quakeRec.isVisibleOnMap) { continue; }

            var quake_data = [];

            //grab just the relevant information
            quake_data.push(quakeRec.eventId);
            quake_data.push(quakeRec.quakeUTCString);
            quake_data.push(quakeRec.depthKM);
            quake_data.push(quakeRec.mag);
            quake_data.push(quakeRec.lat);
            quake_data.push(quakeRec.long);
            if (quakeRec.distances)
                quake_data.push(quakeRec.distances[0])
            else
                quake_data.push('');

            csvContent+=quake_data.join(',')+'\n';
        }
        
        monUtils.downloadCSV(csvContent,'earthquakes.csv','text/csv;encoding:utf-8')

    }

}(window.monQuakeData = window.monQuakeData || {}, jQuery));
