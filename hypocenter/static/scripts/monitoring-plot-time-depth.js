// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotTimeDepth, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, dateTicks = 3,
        maxMag = -99.0, minMag = 99.0, maxDepth = -99.0, minDepth = 99.0, minTime = 0, maxTime = 0; // boundary values

    var chart = {
              'PADT': 5, 'PADR': 5, 'PADB': 20, 'PADL': 40, 'canvasH': 0, 'canvasW': 0, 'canvas': {}, 'context': {}
            };
        
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotTimeDepth.renderTimeDepth = function() {
        if (!isInitialized) {
            isInitialized = true;
        }

        //nothing to do if no points in polygon
        if (monPlotChart.quakesInPolygon.length<=0) { return; }

        var sizes=[];
        var magnitudes=[];
        var depths=[];
        var dates=[];
        var ages=[];
        var distances=[];
        var hover_text=[];
        const records=[];
        
        for(var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {
            
            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; } //only look at quakes that are visible on the map

            records.push(quakeRec);
            //distance
            var distKM=Number(quakeRec.distFromStartKM)
            distances.push(distKM);
            var td_hover="Magnitude: "
            // Magnitude
            var mag=Number(quakeRec.mag);
            magnitudes.push(mag);
            td_hover+=mag;
            var marker_size=3.75*mag+8;
            if ( marker_size<5 ) { marker_size=5; }
            if ( marker_size>20 ) { marker_size=20; }
            sizes.push(marker_size); 
            if (maxMag < parseFloat(quakeRec.mag)) { maxMag = parseFloat(quakeRec.mag); }
            if (minMag > parseFloat(quakeRec.mag)) { minMag = parseFloat(quakeRec.mag); }
            // Depth KM
            var dep=Number(quakeRec.depthKM);
            td_hover+="<br>Depth: "+dep;
            depths.push(dep);
            hover_text.push(td_hover);
            if (maxDepth < parseFloat(quakeRec.depthKM)) { maxDepth = parseFloat(quakeRec.depthKM); }
            if (minDepth > parseFloat(quakeRec.depthKM)) { minDepth = parseFloat(quakeRec.depthKM); }
            // Time (use unix time value, e.g. 1489887407527 ms from 1970.)
            dates.push(quakeRec.date.toISOString());
            ages.push(quakeRec.realAgeDays);
    
            if (maxTime === 0) { maxTime = quakeRec.date.getTime(); }
            if (minTime === 0) {
                minTime = quakeRec.date.getTime();
            }
            if (maxTime < quakeRec.date.getTime()) { maxTime = quakeRec.date.getTime(); }
            if (minTime > quakeRec.date.getTime()) {
                minTime = quakeRec.date.getTime();
            }
        }

        var num_quakes=depths.length;


        var maxQuakeDate=new Date(maxTime)

        // Remove time components from date values.
        minTime = monUtils.removeTime(new Date(minTime)).getTime();
        maxTime = monUtils.removeTime(monUtils.addDays(new Date(maxTime), 1)).getTime();
        
        var minQuakeDate=new Date(minTime)

        var timeDepthLayout={
            title:{
                'text': num_quakes + ' Total Earthquakes',
                xref: 'paper',
                x: 0
            },
            hovermode:'closest',
            autosize:false,
            width: 700,
            height:400,
            margin:{
                l:25,
                r:10,
                b:25,
                t:25,
                pad:0
            },
            plot_bgcolor:"#EFF7F7",
            xaxis:{
                zeroline:false,
                showline:true,
                mirror:true,
                ticks:'outside'
            },
            yaxis:{
                autorange: 'reversed',
                tick0: -3,
                dtick: 3,
                zeroline:false,
                showline:true,
                mirror:true,
                ticks:'outside'
            }
        }

        var timeDepthData={
            type:'scatter',
            mode:'markers',
            x: dates,
            y: depths,
            records:records,
            text: hover_text,
            marker:{
                color:'white',
                opacity:.75,
                line:{
                    color:'rgb(255,128,0)',
                    width:2,
                    opacity:1
                },
                size: sizes
            }
        }

        var timeMagnitudeLayout=monUtils.clone(timeDepthLayout)
        timeMagnitudeLayout.yaxis.autorange=false
        timeMagnitudeLayout.showlegend=false

        //figure out the bar length for the time/magnitude plot
        var minMag=Math.min(...magnitudes);
        var maxMag=Math.max(...magnitudes);
        var minDate=dates[0]
        var maxDate=dates[0]
        for (var i=1;i<dates.length; i++){
            if (dates[i]>maxDate) { maxDate=dates[i]; }
            if (dates[i]<minDate) { minDate=dates[i]; }
        }
        var barWidth=(new Date(maxDate).getTime()-new Date(minDate).getTime())/250;
        
        timeMagnitudeLayout.yaxis.range=[minMag,maxMag+1];
        
        var barLength=[]
        for (var idx=0;idx<magnitudes.length;idx++){
            barLength.push(magnitudes[idx]-minMag);
        }

        var timeMagnitudeData=[{
            type:'scatter',
            mode:'markers',
            x: dates,
            y: magnitudes,
            records:records,
            marker:{
                color:'white',
                opacity:.75,
                line:{
                    color:'rgb(255,128,0)',
                    width:2,
                    opacity:1
                },
                size: sizes
            }
        },{
            type:'bar',
            x:dates,
            y:barLength,
            records:records,
            base:minMag,
            width:barWidth
        }]

        var crossSectionLayout=monUtils.clone(timeDepthLayout)
        crossSectionLayout.xaxis.ticksuffix="Km"

        //TODO: Figure out colorscale for cross-section plot
        // const minAge=Math.min(...ages)
        // const maxAge=Math.max(...ages);
        // const ageRange=maxAge-minAge;
        for(let i=0;i<ages.length;i++){
            ages[i]=-1*ages[i];
        }

        var crossSectionData={
            type:'scatter',
            mode:'markers',
            x: distances,
            y: depths,
            records:records,
            marker:{
                color:ages,
                colorscale: [
                    [0,'rgb(0,0,255)'],
                    [1,'rgb(255,0,0)']
                ],
                opacity:.75,
                colorbar:{
                    thickness:20,
                    title:'Age (days)'
                },
                line:{
                    color:ages,
                    colorscale: [
                        [0,'rgb(0,0,255)'],
                        [1,'rgb(255,0,0)']
                    ],
                    width:2.5,
                    opacity:1
                },
                size: sizes
            }
        }

        var td_config={
            modeBarButtonsToRemove: ['hoverClosestCartesian',
                                     'hoverCompareCartesian',
                                     'lasso2d',
                                     'select2d',
                                     'resetScale2d'],
            displayModeBar: true
        }
        
        var cs_config=Object.assign({},td_config)
        var tm_config=Object.assign({},td_config)
        
        var volcano=$('#volcanoSelect').val()
        var date_from=minQuakeDate.getFullYear()+'-'+(minQuakeDate.getMonth()+1)+'-'+minQuakeDate.getDate()
        var date_to=maxQuakeDate.getFullYear()+'-'+(maxQuakeDate.getMonth()+1)+'-'+maxQuakeDate.getDate()
        var td_filename=volcano+'_time-depth_'+date_from+'_'+date_to+'.png'
        var cs_filename=volcano+'_cross-section_'+date_from+'_'+date_to+'.png'
        var tm_filename=volcano+'_time-mag_'+date_from+'_'+date_to+'.png'
        
        td_config['toImageButtonOptions']={'filename':td_filename}
        cs_config['toImageButtonOptions']={'filename':cs_filename}
        tm_config['toImageButtonOptions']={'filename':tm_filename}

        if (monPlotChart.quakesInPolygon.length>0){
            Plotly.newPlot('timeDepthPlotArea',[timeDepthData],timeDepthLayout,td_config);
            Plotly.newPlot('crossSectionPlotArea',[crossSectionData],crossSectionLayout,cs_config);
            Plotly.newPlot('timeMagnitudePlotArea',timeMagnitudeData,timeMagnitudeLayout,tm_config);
        }
    };
    
}(window.monPlotTimeDepth = window.monPlotTimeDepth || {}, jQuery))
