// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotCum, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isInitialized = false, chartData = [];

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotCum.renderCumMag = function () {

        if (!isInitialized) {
            isInitialized = true;
        }

        if (monPlotChart.quakesInPolygon.length <= 0) //nothing to plot
            return;

        chartData = [];
        var minDate = 999999999999999;
        var maxDate = 0;

        for (var idx = 0; idx < monPlotChart.quakesInPolygon.length; idx++) {

            var quakeRec = monPlotChart.quakesInPolygon[idx];
            if (quakeRec.isFiltered) { continue; }

            chartData.push({
                'time': quakeRec.date,
                'mag': quakeRec.mag,
                'record':quakeRec
            });

            if (quakeRec.date < minDate) {
                minDate = quakeRec.date
            }

            if (quakeRec.date > maxDate) {
                maxDate = quakeRec.date
            }
        }

        // Put values in correct order, time asc.
        chartData.sort(function (a, b) { return (a.time > b.time) ? 1 : ((b.time > a.time) ? -1 : 0); });

        let runningEnergyTotal = 0.0;
        const date_values = []
        const cum_mag_values = [] //cumulative magnitude
        const cum_energy_values = [];
        const cum_total_values = [] //cumulative number of events
        const records=[]
        for (var idx = 0; idx < chartData.length; idx++) {
            var data = chartData[idx]
            records.push(data.record);
            //Convert to energy so we can simply add values
            runningEnergyTotal += Math.pow(10, 4.8 + 1.5 * (data.mag));
            date_values.push(data.time)

            cum_energy_values.push(runningEnergyTotal);
            //convert the total energy back to magnitude for display
            cum_mag_values.push(Math.round(((Math.log10(runningEnergyTotal) - 4.8) / 1.5) * 100) / 100)

            //the index, +1, is the number of events seen
            cum_total_values.push(idx + 1);
        }

        var total_eqs = cum_total_values[cum_total_values.length - 1];

        var cumLayout = {
            title: {
                'text': total_eqs + ' Total Earthquakes',
                xref: 'paper',
                x: .15
            },
            hovermode: 'closest',
            autosize: false,
            width: 700,
            height: 400,
            margin: {
                l: 40,
                r: 15,
                b: 25,
                t: 25,
                pad: 0
            },
            plot_bgcolor: "#EFF7F7",
            xaxis: {
                zeroline: false,
                showline: true,
                mirror: true,
                ticks: 'outside'
            },
            yaxis: {
                zeroline: false,
                showline: true,
                mirror: true,
                ticks: 'outside'
            }
        }

        var cumMagData = {
            name: 'Cum. Mag.',
            visible: $('#magEnergy').val() == 0,
            type: 'scatter',
            mode: 'lines',
            x: date_values,
            y: cum_mag_values,
            records:records,
            line: {
                shape: 'hv',
                color: 'rgb(237,196,69)'
            }
        }

        var cumEnergyData = {
            name: 'Cum. Energy.',
            type: 'scatter',
            mode: 'lines',
            visible: $('#magEnergy').val() == 1,
            x: date_values,
            y: cum_energy_values,
            records:records,
            line: {
                shape: 'hv',
                color: 'rgb(237,196,69)'
            }
        }
        var total_hover = []
        for (var i = 0; i < cum_total_values.length; i++) {
            var cur = cum_total_values[i];
            var remain = total_eqs - cur;
            var text = cur + '<-->' + remain;
            total_hover.push(text);
        }

        var cumTotalData = {
            type: 'scatter',
            mode: 'lines',
            text: total_hover,
            x: date_values,
            y: cum_total_values,
            records:records,
            line: {
                shape: 'hv',
                color: 'rgb(237,196,69)'
            }
        }

        var volcano = $('#volcanoSelect').val()
        var date_from = minDate.getFullYear() + '-' + minDate.getMonth() + '-' + minDate.getDate()
        var date_to = maxDate.getFullYear() + '-' + maxDate.getMonth() + '-' + maxDate.getDate()
        var mag_filename = volcano + '_cumulative-mag_' + date_from + '_' + date_to + '.png'
        var total_filename = volcano + '_cumulative-total_' + date_from + '_' + date_to + '.png'

        var mag_config = {
            modeBarButtonsToRemove: [
                'lasso2d',
                'select2d',
                'resetScale2d'],
            displayModeBar: true
        }

        var total_layout = { ...cumLayout }
        delete total_layout['hovermode'];
        total_layout['title'] = {
            'text': total_eqs + ' Total Earthquakes',
            xref: 'paper',
            x: 0
        };

        var total_config = { ...mag_config };
        total_config['toImageButtonOptions'] = { 'filename': total_filename }
        mag_config['toImageButtonOptions'] = { 'filename': mag_filename }


        if (monPlotChart.quakesInPolygon.length > 0) {
            Plotly.newPlot('cumMagPlotArea', [cumMagData, cumEnergyData], cumLayout, mag_config);
            Plotly.newPlot('cumTotalPlotArea', [cumTotalData], total_layout, total_config);
        }

    };

}(window.monPlotCum = window.monPlotCum || {}, jQuery));
