// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
/* global dojo, esri */
(function (monPlotMap, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var isSelectingPoints = false, recPoly = {}, polygonPoints = [];
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monPlotMap.mapLayer = {};
    monPlotMap.p1 = {};
    monPlotMap.p2 = {};
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function addPointToLayer(point) {

        var symbol = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_CROSS,
            20,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_CROSS, new dojo.Color('red'), 1),
            new dojo.Color('red')
        );
        var graphic = new esri.Graphic(point, symbol,{graphicType:'plotMarker',
                                                      pointNum:point.pointnum});
        monPlotMap.mapLayer.add(graphic);

        var textSymbol =  new esri.symbol.TextSymbol((point.pointnum === 1 ? 'A' : 'B'))
                                .setColor(new dojo.Color('red'))
                                .setAlign(esri.symbol.Font.ALIGN_START)
                                .setOffset(8, 8)
                                .setVerticalAlignment('middle')
                                .setFont(
                                    new esri.symbol.Font('12pt')
                                    .setFamily('Arial')
                                    .setWeight(esri.symbol.Font.WEIGHT_BOLD)
                                );
                    
        var textGraphic = new esri.Graphic(point, textSymbol,{graphicType:'plotMarker',
                                                              pointNum:point.pointnum});
        monPlotMap.mapLayer.add(textGraphic);
    }

    // Lifted from VOLCWEB's xsec.js, then modified a bit. Rectangle logic by Weston Thelen and Scott Pincus.
    function renderPolygonAndSelectQuakes(callback) { 

        // Reset the layer, this may be called from several different functions.
        if (!monPlotMap.p1.x || !monPlotMap.p2.x) { return; }
        monPlotMap.mapLayer.clear();
        addPointToLayer(monPlotMap.p1);
        addPointToLayer(monPlotMap.p2);
        recPoly = {};
        polygonPoints = [];
        
        //
        var fullWidth = $('#vscmmPlotWidth').val();
        var halfWidth = (parseFloat(fullWidth) ? fullWidth / 2 : 5); // W is 1/2 width in KM, defaults to 5.
        halfWidth *=1000; //convert to meters for calculations

        // Get angle of cross section line
        var phirad = Math.atan(((monPlotMap.p2.y - monPlotMap.p1.y) / (monPlotMap.p2.x - monPlotMap.p1.x))); // radians
        var phideg = Math.abs((180 / Math.PI) * phirad);		//convert from radians to degrees for debugging

        // Adjust based on which sector was clicked first------------------------------------------
        // x coordinate increases to the right, and the y coordinate increases downwards
        var LLUR = 0, ULLR = 0, LRUL = 0, URLL = 0, locTemp = {};
        if (monPlotMap.p1.x <= monPlotMap.p2.x && monPlotMap.p1.y >= monPlotMap.p2.y) { ULLR = 1; } // Upper L to lower R
        if (monPlotMap.p1.x <= monPlotMap.p2.x && monPlotMap.p1.y <= monPlotMap.p2.y) { LLUR = 1; } // Lower L to upper R
        if (monPlotMap.p1.x > monPlotMap.p2.x && monPlotMap.p1.y >= monPlotMap.p2.y ) { // Upper right to lower left
            URLL = 1;
            locTemp = monPlotMap.p2; //swap points so it acts like ULLR
            monPlotMap.p2 = monPlotMap.p1;
            monPlotMap.p1 = locTemp;
        }
        if (monPlotMap.p1.x > monPlotMap.p2.x && monPlotMap.p1.y <= monPlotMap.p2.y) { // Lower right to upper left
            LRUL = 1;
            locTemp = monPlotMap.p2; //swap points so it acts like LLUR
            monPlotMap.p2 = monPlotMap.p1;
            monPlotMap.p1 = locTemp;
        }	

        // Calculate distance ratio in map/spatial coords 
        // monPlotMap.p1 is in map/spatial coords of a user clicked event. monPlotMap.p1 set in calling function
        var p1 = new esri.geometry.Point(monPlotMap.p1);
        var p2 = new esri.geometry.Point(monPlotMap.p2);

        //we can't do the math in webMercator due to distortions, so convert to UTM via lat/long
        //TODO: determine if we can go directly
        p1=esri.geometry.webMercatorToGeographic(p1); //now in lat/long
        var p1UTM=UTMUtils.LatLonToUTMXY(p1.y, p1.x);

        p2=esri.geometry.webMercatorToGeographic(p2);
        var p2UTM=UTMUtils.LatLonToUTMXY(p2.y,p2.x);

        //now that p1 and p2 are in UTM, we can add meters to them correctly

        // Find x and y offsets (in pixels)-----------------------------------------
        // Absolute value allows me to be even sloppier with my math
        var dely = Math.abs(halfWidth * Math.sin((Math.PI / 180) * (90 - phideg)));	//Convert to radians dummy
        var delx = Math.abs(halfWidth * Math.cos((Math.PI / 180) * (90 - phideg)));

        // Get rectangle vertex points---------------------------------------------------------------------------
        // In pixels (despite name)
        //Point one is furthest south, and subsequent points are ccw
        if (ULLR === 1 || LRUL === 1) {
            var vert1=new esri.geometry.Point(p2)
            var vert1UTM = monUtils.clone(p2UTM);//new esri.geometry.Point(p2);
            vert1UTM[0]=(vert1UTM[0] - delx);
            vert1UTM[1]=(vert1UTM[1] - dely);
            var vert2=new esri.geometry.Point(p2)
            var vert2UTM = monUtils.clone(p2UTM);
            vert2UTM[0]=(vert2UTM[0] + delx);
            vert2UTM[1]=(vert2UTM[1] + dely);
            var vert3=new esri.geometry.Point(p1)
            var vert3UTM = monUtils.clone(p1UTM);
            vert3UTM[0]=(vert3UTM[0] + delx);
            vert3UTM[1]=(vert3UTM[1] + dely);
            var vert4=new esri.geometry.Point(p1)
            var vert4UTM = monUtils.clone(p1UTM);
            vert4UTM[0]=(vert4UTM[0] - delx);
            vert4UTM[1]=(vert4UTM[1] - dely);
            if (LRUL === 1){		// swap points back from above
                locTemp = monPlotMap.p2;
                monPlotMap.p2 = monPlotMap.p1;
                monPlotMap.p1 = locTemp;
            }
        }

        if (LLUR === 1 || URLL === 1) {
            var vert1=new esri.geometry.Point(p2)
            var vert1UTM = monUtils.clone(p2UTM);
            vert1UTM[0]=(vert1UTM[0] + delx);
            vert1UTM[1]=(vert1UTM[1] - dely);
            var vert2=new esri.geometry.Point(p1)
            var vert2UTM = monUtils.clone(p1UTM);
            vert2UTM[0]=(vert2UTM[0] + delx);
            vert2UTM[1]=(vert2UTM[1] - dely);
            var vert3=new esri.geometry.Point(p1)
            var vert3UTM = monUtils.clone(p1UTM);
            vert3UTM[0]=(vert3UTM[0] - delx);
            vert3UTM[1]=(vert3UTM[1] + dely);
            var vert4=new esri.geometry.Point(p2)
            var vert4UTM = monUtils.clone(p2UTM);
            vert4UTM[0]=(vert4UTM[0] - delx);
            vert4UTM[1]=(vert4UTM[1] + dely);
            if (URLL === 1){		// swap points back from above
                locTemp = monPlotMap.p2;
                monPlotMap.p2 = monPlotMap.p1;
                monPlotMap.p1 = locTemp;
            }
        }

        //convert back to map coordinates
        //start with lat/long
        var v1LatLon=UTMUtils.UTMXYToLatLon(...vert1UTM)
        var v2LatLon=UTMUtils.UTMXYToLatLon(...vert2UTM)
        var v3LatLon=UTMUtils.UTMXYToLatLon(...vert3UTM)
        var v4LatLon=UTMUtils.UTMXYToLatLon(...vert4UTM)

        vert1.setX(v1LatLon[1])
        vert1.setY(v1LatLon[0])
        vert2.setX(v2LatLon[1])
        vert2.setY(v2LatLon[0])
        vert3.setX(v3LatLon[1])
        vert3.setY(v3LatLon[0])
        vert4.setX(v4LatLon[1])
        vert4.setY(v4LatLon[0])

        //convert lat/lon back to webMercator
        vert1=esri.geometry.geographicToWebMercator(vert1);
        vert2=esri.geometry.geographicToWebMercator(vert2);
        vert3=esri.geometry.geographicToWebMercator(vert3);
        vert4=esri.geometry.geographicToWebMercator(vert4);

        //Create resize handles
        if (ULLR === 1 || LRUL === 1) {
            var halfx_left=(vert4.x-vert1.x)/2;
            var halfy_left=(vert4.y-vert1.y)/2;
            var halfx_right=(vert3.x-vert2.x)/2;
            var halfy_right=(vert3.y-vert2.y)/2;
            var left_handle_point=new esri.geometry.Point(vert1);
            var right_handle_point=new esri.geometry.Point(vert2);
        } else {
            var halfx_left=(vert2.x-vert1.x)/2;
            var halfy_left=(vert2.y-vert1.y)/2;
            var halfx_right=(vert3.x-vert4.x)/2;
            var halfy_right=(vert3.y-vert4.y)/2;
            var left_handle_point=new esri.geometry.Point(vert1);
            var right_handle_point=new esri.geometry.Point(vert4);
        }

        left_handle_point.setX(left_handle_point.x+halfx_left);
        left_handle_point.setY(left_handle_point.y+halfy_left);
        right_handle_point.setX(right_handle_point.x+halfx_right);
        right_handle_point.setY(right_handle_point.y+halfy_right);

        monPlotMap.rp1=left_handle_point;
        monPlotMap.rp2=right_handle_point;

        var symbol = new esri.symbol.SimpleMarkerSymbol(
            esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND,
            20,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DIAMOND, new dojo.Color('green'), 1),
            new dojo.Color('green')
        );
        var left_graphic = new esri.Graphic(left_handle_point, symbol,{graphicType:'resizeMarker',
                                                      side:1});

        var right_graphic = new esri.Graphic(right_handle_point, symbol,{graphicType:'resizeMarker',
                                                      side:2});
        monPlotMap.mapLayer.add(right_graphic);
        monPlotMap.mapLayer.add(left_graphic);

        //draw the polygon defined by the points and the cross section width
        polygonPoints = [vert3, vert2, vert1, vert4, vert3];

        recPoly = new esri.geometry.Polygon(monMap.map.spatialReference);
        recPoly.addRing(polygonPoints);

        var recOutline = new esri.symbol.SimpleLineSymbol();
        recOutline.setColor (new dojo.Color('black'), 1);
        recOutline.setWidth(.25);

        var recSymbol = new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID, 
                recOutline, 
                new dojo.Color([255, 255, 0, 0.25])
            );
    
        monPlotMap.mapLayer.add(new esri.Graphic(recPoly, recSymbol));

        if (monPlotMap.selectedGraphic || monPlotMap.selectedResize) { return; }
        var pointA = (monPlotMap.p1.pointnum === 1 ? monPlotMap.p1 : monPlotMap.p2);
        // Set data for quakes inside the polygon.
        monPlotChart.quakesInPolygon = [];
        for (var idx = 0; idx < monQuakeData.quakeRecs.length; idx++) {
            var quakeRec = monQuakeData.quakeRecs[idx];
            if (quakeRec.isFiltered) { continue; }
            var point = new esri.geometry.Point(quakeRec.long, quakeRec.lat);
    		if (recPoly.contains(point)) {
                var d = monUtils.distanceM(pointA.getLatitude(), pointA.getLongitude(), quakeRec.lat, quakeRec.long);
                quakeRec.distFromStartKM = monUtils.round(d / 1000, 3);
                monPlotChart.quakesInPolygon.push(quakeRec);
            }
        }

        $('#plotGenerating').show();
        $('.plot-ready').hide();
        
        //run asynchrounously so we can draw the polygon instantly
        setTimeout(renderPlots,0);

        if (typeof callback === 'function') { callback(); }
    }

    function renderPlots(){
        monPlotChart.renderAll();
        $('.plot-ready').show();
        $('#plotGenerating').hide();
    }

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monPlotMap.enableCapture = function() {
        
        $('.plot-active').show();
        $('.plot-inactive').hide();
        $('.plot-ready').hide();
        isSelectingPoints = true;
        monPlotMap.mapLayer.clear();
        monPlotMap.p1 = {};
        monPlotMap.p2 = {};
    };

    monPlotMap.redraw = function() {

        renderPolygonAndSelectQuakes();
    };
    
    monPlotMap.incrementWidth = function(incVal) {
        
        var widthVal = parseFloat($('#vscmmPlotWidth').val());
        if (!widthVal || widthVal + incVal < 0) {
            widthVal = 1;
        } else {
            widthVal = widthVal + incVal;
        }
        $('#vscmmPlotWidth').val(widthVal);
        monPlotMap.redraw();
    };
    
    monPlotMap.reset = function() {
        
        $('.plot-active').hide();
        $('.plot-inactive').show();
        $('.plot-ready').hide();
        isSelectingPoints = false;
        monPlotMap.mapLayer.clear();
        monPlotMap.p1 = {};
        monPlotMap.p2 = {};
        recPoly = {},
        polygonPoints = [],
        monPlotChart.quakesInPolygon = [];
    };

    monPlotMap.moveMarker = function(event){
        if (!monPlotMap.selectedGraphic && !monPlotMap.selectedResize){ return; }
        var point;

        if(monPlotMap.selectedGraphic){
            if (monPlotMap.selectedGraphic.attributes.pointNum==1) { point=monPlotMap.p1; }
            else { point=monPlotMap.p2; }

            point.setX(event.mapPoint.x);
            point.setY(event.mapPoint.y);
        }
        else{
            //resize
            var point1;
            var point2;
            if(monPlotMap.selectedResize.attributes.side==1){
                monPlotMap.rp1.setX(event.mapPoint.x);
                monPlotMap.rp1.setY(event.mapPoint.y);
                point1=monPlotMap.rp1;
                point2=monPlotMap.rp2;
            }else{
                monPlotMap.rp2.setX(event.mapPoint.x);
                monPlotMap.rp2.setY(event.mapPoint.y);
                point1=monPlotMap.rp2;
                point2=monPlotMap.rp1;
            }
            var old_dist=$('#vscmmPlotWidth').val();

            //to get the *correct* distance, we need to convert to UTM via lat/long
            point1=esri.geometry.webMercatorToGeographic(point1);
            point2=esri.geometry.webMercatorToGeographic(point2);
            var rp1UTM=UTMUtils.LatLonToUTMXY(point1.y,point1.x);
            var rp2UTM=UTMUtils.LatLonToUTMXY(point2.y,point2.x);
            
            //Abuse the system! we can set x and y to whatever we want, and the getLength function
            //just applies the Pythagorean theorem therom to those values. So the fact that the
            //Spatial reference in the points is incorrect doesn't affect us.
            point1.setX(rp1UTM[0])
            point1.setY(rp1UTM[1])
            point2.setX(rp2UTM[0])
            point2.setY(rp2UTM[1])

            var new_dist=esri.geometry.getLength(point1, point2) / 1000;

            var dist_change=new_dist-old_dist;
            new_dist=Math.abs(new_dist+dist_change)
            new_dist=Math.round(new_dist*100)/100
            //twice the change.
            $('#vscmmPlotWidth').val(new_dist);
        }
        renderPolygonAndSelectQuakes();
    }

    monPlotMap.markerMoved = function(){
        if (!monPlotMap.selectedGraphic && !monPlotMap.selectedResize) { return; }
        
        monMap.map.enablePan();
        monPlotMap.selectedGraphic=null;
        monPlotMap.selectedResize=null;
        renderPolygonAndSelectQuakes();
    }

    monPlotMap.init = function (){
                
        // Set vscmmPlotWidth input value based on map zoom
        if (monUiOptions.mapZoom > 12) {
            $('#vscmmPlotWidth').val(1);
        } if (monUiOptions.mapZoom === 12) {
            $('#vscmmPlotWidth').val(5);
        } if (monUiOptions.mapZoom < 12) {
            $('#vscmmPlotWidth').val(10);
        }
        
        $('.plot-active').hide();
        $('.plot-inactive').show();
        $('.plot-ready').hide();
        
        monPlotMap.mapLayer = new esri.layers.GraphicsLayer();
        monMap.map.addLayer(monPlotMap.mapLayer);
        monMap.map.on('mouse-drag',monPlotMap.moveMarker);
        monMap.map.on('mouse-drag-end',monPlotMap.markerMoved);
        monMap.map.on('click', function(event) {
            if (!isSelectingPoints) { return; }
            var clickPoint = event.mapPoint;
            if (!monPlotMap.p1.x) {
                monPlotMap.p1 = clickPoint;
                monPlotMap.p1.pointnum = 1;
                addPointToLayer(clickPoint);
            } else if (!monPlotMap.p2.y) {
                monPlotMap.p2 = clickPoint;
                monPlotMap.p2.pointnum = 2;
                addPointToLayer(clickPoint);
                renderPolygonAndSelectQuakes();
                //isSelectingPoints = false;
            } else {
                if(event.graphic && event.graphic.attributes && event.graphic.attributes.graphicType=="plotMarker"){
                    monMap.map.disablePan();
                    monPlotMap.selectedGraphic=event.graphic;
                    return;
                }
                else if (event.graphic && event.graphic.attributes && event.graphic.attributes.graphicType=="resizeMarker"){
                    monMap.map.disablePan();
                    monPlotMap.selectedResize=event.graphic;
                    console.log("Clicked Resize")
                    return;
                }
                else {
                    if(monPlotMap.selectedGraphic){
                        monMap.map.enablePan();
                        monPlotMap.selectedGraphic=null;
                        return;
                    }
                }
                monPlotMap.mapLayer.clear();
                monPlotMap.p1 = clickPoint;
                monPlotMap.p1.pointnum = 1;
                addPointToLayer(clickPoint);
                monPlotMap.p2 = {};
            }
        });
    };
    
}(window.monPlotMap = window.monPlotMap || {}, jQuery));
