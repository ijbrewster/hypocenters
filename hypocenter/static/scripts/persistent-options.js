// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (persistentOptions, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var bundle = { // These are the default values if not set elsewhere.
        categoryOptions           : {},
        kmOrMi                    : 'km',    // mi | km
        utcOrLocal                : 'UTC', // local | UTC
        quakeColorChoice          : 'time',  // depth | time,
        esriBasemapId             : '',      // Value from basemap chooser
        isHideAllQuakes           : false,   // true | false (boolean)
        quakeAgeSelection         : 'any',
        quakeMagToggle            : false,   // Forces object creation in quake-ui, this is the default
        quakeDepthToggle          : false,   // Forces object creation in quake-ui, this is the default
        legendSetting             : 'app',   // app | on | off
        automaticData             : 'off'    // on | off
    };
    
    var timer = null;

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    function save() { // Serialize bundle object to local storage.

        if (typeof(Storage) === 'undefined') { return; }
        if (timer) { clearTimeout(timer); }
        timer = setTimeout(function() {
            localStorage.setItem('persistentOptions.bundle', JSON.stringify(bundle));
        }, 1000); // Prevents saving too frequently if bundle is changing rapidly.
    }
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    persistentOptions.put = function(item, value) {
        
        bundle[item] = value;
        save();
    };
    
    persistentOptions.get = function(item) {
        
        return bundle[item];
    };
    
    persistentOptions.restore = function() {

        if (typeof(Storage) === 'undefined') { return; }
        if (localStorage.getItem('persistentOptions.bundle')) {
            bundle = JSON.parse(localStorage.getItem('persistentOptions.bundle'));
        }
    };
    
    persistentOptions.reset = function() {

        if (typeof(Storage) !== 'undefined') { localStorage.removeItem('persistentOptions.bundle'); }
        window.location.reload();
    };
    
}(window.persistentOptions = window.persistentOptions || {}, jQuery));
