// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

(function (monInstrumentData, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    var dataParams = '', outputId = 1;

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monInstrumentData.instrumentRecs = [];
    monInstrumentData.instrumentCategories = [];
    monInstrumentData.instrumentTypes = [];
    monInstrumentData.hasApproximateLocations = true;

    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    function processDataAfterPull() {

        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) { // zero each category total.
            monInstrumentData.instrumentCategories[idx].categoryTotal = 0;
        }

        for (var idx = 0; idx < monInstrumentData.instrumentRecs.length; idx++) { // Process each instrument.

            // Add to category total.
            for (var idx2 = 0; idx2 < monInstrumentData.instrumentCategories.length; idx2++) { 
                if (monInstrumentData.instrumentCategories[idx2].catId === 
                        monInstrumentData.instrumentRecs[idx].catId) {
                    monInstrumentData.instrumentCategories[idx2].categoryTotal++;
                }
            }
            
            // Add catRec to instrument.
            monInstrumentData.instrumentRecs[idx].catRec = 
                    monInstrumentData.getCategory(monInstrumentData.instrumentRecs[idx].catId);
            
            // Add typeRec to instrument.
            monInstrumentData.instrumentRecs[idx].typeRec = getType(monInstrumentData.instrumentRecs[idx].typeId);
            
            // Assign local outputId for instrument output lookups.
            for (var idx2 = 0; idx2 < monInstrumentData.instrumentRecs[idx].output.length; idx2++) {
                monInstrumentData.instrumentRecs[idx].output[idx2].outputId = outputId;
                outputId++;
            }
        }
        
        // Sort categories on category
        monInstrumentData.instrumentCategories.sort(function(a, b) { return a['category'] > b['category']; });
    }
    
    function getInstruments(minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {

        var params = '?lat1=' + minLatIn + '&long1=' + minLongIn + '&lat2=' + maxLatIn + '&long2=' + maxLongIn;
        $.getJSON(monUiOptions.instrumentsUrl + params, function(data) {
      
            monInstrumentData.hasApproximateLocations = (data.positioning === 'rounded');
            
            monInstrumentData.instrumentRecs = monInstrumentData.instrumentRecs.concat(data.instruments);
            
            for (var idx = 0; idx < data.categories.length; idx++) { // Add missing categories.
                if (!monInstrumentData.getCategory(data.catId)) {
                    if (data.categories[idx].iconUrl.indexOf('https://volcanoes.usgs.gov') < 0) {
                        data.categories[idx].iconUrl = 'https://volcanoes.usgs.gov' + data.categories[idx].iconUrl;
                    }
                    monInstrumentData.instrumentCategories.push(data.categories[idx]);
                }
            }
     
            for (var idx = 0; idx < data.types.length; idx++) { // Add missing types.
                if (!getType(data.typeId)) {
                    monInstrumentData.instrumentTypes.push(data.types[idx]);
                }
            }
            
            if (typeof callback === 'function') { callback(); }
        });
    }
    
    function getType(typeId) {

        for (var idx = 0; idx < monInstrumentData.instrumentTypes.length; idx++) {
            if (parseInt(monInstrumentData.instrumentTypes[idx].typeId, 10) === parseInt(typeId, 10)) {
                return monInstrumentData.instrumentTypes[idx];
            }
        }
        return false;
    };
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monInstrumentData.getCategory = function(catId) {

        for (var idx = 0; idx < monInstrumentData.instrumentCategories.length; idx++) {
            if (parseInt(monInstrumentData.instrumentCategories[idx].catId, 10) === parseInt(catId, 10)) {
                return monInstrumentData.instrumentCategories[idx];
            }
        }
        return false;
    };

    monInstrumentData.getInstrumentsIfNeeded = function (minLongIn, minLatIn, maxLongIn, maxLatIn, callback) {

        var params = '?lat1=' + minLatIn + '&long1=' + minLongIn + '&lat2=' + maxLatIn + '&long2=' + maxLongIn;
        if (params === dataParams) { return; }
        dataParams = params;
        
        monInstrumentData.instrumentRecs = [];
        monInstrumentData.instrumentCategories = [];
        monInstrumentData.instrumentTypes = [];
        monInstrumentData.hasApproximateLocations = true;
        
        if (monUiOptions.mapZoom < monUiOptions.zoomThreshold) {
            processDataAfterPull();
            if (typeof callback === 'function') { callback(); }
            return;
        }

        getInstruments(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {

           if (minLongIn < -180 || maxLongIn < -180) { // Crossing the date line, moving west. Do second data pull.

                if (minLongIn < -180 && maxLongIn > -180) {
                    maxLongIn = 180;
                    minLongIn = Math.abs(minLongIn) - 180;
                } else {
                    if (minLongIn < -180) { minLongIn = Math.abs(minLongIn) - 180; }
                    if (maxLongIn < -180) { maxLongIn = Math.abs(maxLongIn) - 180; }
                    if (maxLongIn < minLongIn) {
                        var tmp = maxLongIn;
                        maxLongIn = minLongIn;
                        minLongIn = tmp;
                    }
                }

                getInstruments(minLongIn, minLatIn, maxLongIn, maxLatIn, function() {
                    processDataAfterPull();
                    if (typeof callback === 'function') { callback(); }
                });

            } else {

                processDataAfterPull();
                if (typeof callback === 'function') { callback(); }
            }
       });
    };

}(window.monInstrumentData = window.monInstrumentData || {}, jQuery));
