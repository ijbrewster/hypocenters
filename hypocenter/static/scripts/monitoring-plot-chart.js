// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com
(function (monPlotChart, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////
    var isInitialized = false;
    
    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////
    monPlotChart.quakesInPolygon = [];
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////

    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////
    
    monPlotChart.renderAll = function() {
        
        if (!isInitialized) {
            isInitialized = true;
            $('#vscmmPlotModal').draggable({handle:".dialog-header"});
        }
        monPlotTimeDepth.renderTimeDepth();
        monPlotCum.renderCumMag();
        update_cat();
    };
    
}(window.monPlotChart = window.monPlotChart || {}, jQuery));
