// Author: Mike Randall, mjrandall@usgs.gov, mjranda@gmail.com

$(document).ready(function() {
    
    'use strict';
    // Fix for browsers not supporting unselected console.
    if (!window.console) {
        var console = {
           log : function(){},
           warn : function(){},
           error : function(){},
           time : function(){},
           timeEnd : function(){}
        };
    }
});

(function (monUtils, $, undefined) {

    'use strict';

    // Private Properties. /////////////////////////////////////////////////////////////////////////////////////////////

    // Public Properties ///////////////////////////////////////////////////////////////////////////////////////////////

    monUtils.MSDAY = 8.64e+7; // 24 * 60 * 60 * 1000 = Milliseconds per day.
    monUtils.preventAccordion = false;
    
    // Private Methods /////////////////////////////////////////////////////////////////////////////////////////////////
    
    // Public Methods //////////////////////////////////////////////////////////////////////////////////////////////////

    monUtils.getURLParameter = function(pname) {
        // THANKS: https://stackoverflow.com/questions/1403888/get-url-parameter-with-jquery
        var pval = decodeURIComponent((new RegExp('[?|&]' + pname + '=' + '([^&;]+?)(&|#|;|$)')
                .exec(location.search)||[,''])[1].replace(/\+/g, '%20'))||null;
        if (pval === 'null') {
            return null;
        } else {
            return pval;
        }
    };

    /*
        < 0     = bf1e2d - 191, 30, 45
        0 - < 5   = ed1c24 - 237, 28, 36
        5 - < 10  = f7941e - 247, 148, 30
        10 - < 15 = f5ee30 - 245, 238, 48
        15 - < 20 = 2bb673 - 43, 182, 115
        20 - < 40 = 1f99d5 - 31, 153, 213
        > 40    = 662d91 - 102, 45, 145
    */
    monUtils.getDepthColor = function(depthIn) {

        depthIn = parseFloat(depthIn);
        
        if (depthIn < 0) { return '#a42431'; }
        else if (depthIn < 5) { return '#ed1c24'; }
        else if (depthIn < 10) { return '#f7941e'; }
        else if (depthIn < 15) { return '#f5ee30'; }
        else if (depthIn < 20) { return '#2bb673'; }
        else if (depthIn < 40) { return '#1f99d5'; }
        else { return '#662d91'; }
    };

    monUtils.getDepthColorRGB = function(depthIn) {

        depthIn = parseFloat(depthIn);

        if (depthIn < 0) { return [164, 36, 49]; }
        else if (depthIn < 5) { return [237, 28, 36]; }
        else if (depthIn < 10) { return [247, 148, 30]; }
        else if (depthIn < 15) { return [245, 238, 48]; }
        else if (depthIn < 20) { return [43, 182, 115]; }
        else if (depthIn < 40) { return [31, 153, 213]; }
        else { return [102, 45, 145]; }
    };

    /*
    #d31820 - 211, 24, 32   Last 2 hours
    #f7941e - 247, 148, 30  Last 2 days
    #fff200 - 255, 242, 0   Last 2 weeks
    #ffffff - 255, 255, 255 > 2 weeks
     */
    monUtils.getTimeColor = function(ageDays) {

        if (!ageDays) { return '#ffffff'; }
        ageDays = parseFloat(ageDays);

        if (ageDays <= (2 / 24)) { return '#d31820'; }
        else if (ageDays <= 2) { return '#f7941e'; }
        else if (ageDays <= 14) { return '#fff200'; }
        else { return '#ffffff'; }
    };

    monUtils.getTimeColorRGB = function(ageDays) {

        if (!ageDays) { return [255, 255, 255]; }
        ageDays = parseFloat(ageDays);

        if (ageDays <= (2 / 24)) { return [211, 24, 32]; }
        else if (ageDays <= 2) { return [247, 148, 30]; }
        else if (ageDays <= 14) { return [255, 242, 0]; }
        else { return [255, 255, 255]; }
    };

    // Get the diameter for a quake.
    monUtils.getQuakePixelDiameter = function(mag) {

        mag = parseFloat(mag);
        if (isNaN(mag) || mag < 0) { mag = 0; }
        switch (Math.floor(mag)){
            case 0:
                return 6;
                break;
            case 1:
                return 10;
                break;
            case 2:
                return 14;
                break;
            case 3:
                return 18;
                break;
            case 4:
                return 22;
                break;
            case 5:
                return 26;
                break;
            case 6:
                return 30;
                break;
            default:
                return 34;
        }
    };
    
    // Add a leading 0 to single digit numbers.
    function padTime(numberIn) {
        
        var number = parseInt(numberIn, 10);
        return number < 10 ? '0' + number : number;
    }

    monUtils.getUTCOffsetHrs = function(){
        
        var cdate = Date.parse('now');
        var utcO = cdate.getUTCOffset();
        utcO = parseInt(utcO.substr(0, utcO.length -2), 10);
        return utcO;
    };
    
    monUtils.getUTCOffsetMs = function(){

        return monUtils.getUTCOffsetHrs() * 60 * 60 * 1000;
    };
    
    monUtils.unixTimestampToDate = function(unixTimestamp) {
        
        return new Date(unixTimestamp * 1000);
    };

    monUtils.dateToString = function(dateIn) {
        
        return dateIn.getFullYear() + '-' + padTime(dateIn.getMonth() + 1) + '-' + padTime(dateIn.getDate()) + ' ' + 
            padTime(dateIn.getHours()) + ':' + padTime(dateIn.getMinutes()) + ':' + padTime(dateIn.getSeconds());
        //return dateIn.toString();
    };

    monUtils.dateToUTCString = function(dateIn) {
        
        return dateIn.getUTCFullYear() + '-' + padTime(dateIn.getUTCMonth() + 1) + '-' + 
            padTime(dateIn.getUTCDate()) + ' ' + padTime(dateIn.getUTCHours()) + ':' + padTime(dateIn.getUTCMinutes()) + 
            ':' + padTime(dateIn.getUTCSeconds());
        //return dateIn.toISOString();
    };

    // Return date in yy-mm-dd format. dateIn: JavaScript date object, utcOrLocal, string: UTC | local
    monUtils.dateYMD = function(dateIn, utcOrLocal) {
        
        if (utcOrLocal === 'UTC') {
            return dateIn.getUTCFullYear().toString().substr(2) + '-' + padTime(dateIn.getUTCMonth() + 1) + '-' + 
                padTime(dateIn.getUTCDate());
        } else {
            return dateIn.toString('yy-MM-dd');
        }
    }
    
    // Calculates difference in age between two dates, returns difference in Hours:Minutes format.
    monUtils.dateDiffHHMM = function(date1, date2) {

        var diffMS, diffH, diffStr, diffDec, diffMin;

        diffMS = date1.getTime() - date2.getTime();
        if (diffMS === 0) { return '0:00'; }
        diffH = diffMS / 1000 / 60 / 60;
        if (diffH.toString().indexOf('.') < 0) { return diffH + ':00'; }
        diffStr = (diffH.toString().split('.'))[0];
        diffDec = '.' + (diffH.toString().split('.'))[1];
        diffMin = Math.round((parseFloat(diffDec) * 60), 2);
        if (diffMin === 0) {
            diffMin = '00';
        } else if (diffMin < 10) {
            diffMin = '0' + diffMin;
        }
        return diffStr + ':' + diffMin.toString();
    };
    
    // Calculates difference in age between two dates, returns difference in Hours with a decmial for partial hours.
    monUtils.dateDiffHH = function(date1, date2) {

        var diffMS = date1.getTime() - date2.getTime();
        return diffMS / 1000 / 60 / 60;
    };

    // Calculates difference in age between two dates, returns difference in days with a decmial for partial days.
    monUtils.dateDiffDD = function(date1, date2) {

        var diffMS = date1.getTime() - date2.getTime();
        return diffMS / monUtils.MSDAY;
    };
    
    monUtils.removeTime = function(date) {
        
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    };
    
    // Returns new date object with NN days added.
    monUtils.addDays = function(date, daysToAdd) {
        
        return new Date(date.valueOf() + (monUtils.MSDAY * daysToAdd));
    };
    
    // Calculate distance in meters between two geographic points. /////////////////////////////////////////////////////
    // Thanks: https://www.movable-type.co.uk/scripts/latlong.html
    monUtils.distanceM = function(lat1, lon1, lat2, lon2) {
        
        /** Extend Number object with method to convert numeric degrees to radians */
        if (Number.prototype.toRadians === undefined) {
            Number.prototype.toRadians = function() { return this * Math.PI / 180; };
        }

        var R = 6371e3; // Earth's radius in meters
        var φ1 = lat1.toRadians();
        var φ2 = lat2.toRadians();
        var Δφ = (lat2 - lat1).toRadians();
        var Δλ = (lon2 - lon1).toRadians();

        var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        var d = R * c;
        
        return d;
    }
    
    // Load scripts and css elements to DOM ////////////////////////////////////////////////////////////////////////////
    monUtils.loadScripts = function(urls, callback) {

        // urls must be an array with at least one element.
        var unsentTotal = urls.length;
        for (var idx = 0; idx < urls.length; idx++) {
            monUtils.loadScript(urls[idx], function(){
                unsentTotal--;
                if (unsentTotal === 0) { callback(); }
            });
        }
        
    };
    
    monUtils.loadScript = function(url, callback) {

        // Is script already loaded? - Just do callback.
        $('script').each(function() { if ($(this).attr('src') === url) { callback(); } });

        var script = document.createElement('script');
        script.type = 'text/javascript';

        if (script.readyState) {  //IE - TODO, may not be needed with edge.
            script.onreadystatechange = function () {
                if (script.readyState === 'loaded' ||
                        script.readyState === 'complete') {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }
        
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    };

    monUtils.loadStylesheets = function(urls, callback) {

        // urls must be an array with at least one element.
        var unsentTotal = urls.length;
        for (var idx = 0; idx < urls.length; idx++) {
            monUtils.loadStylesheet(urls[idx], function(){
                unsentTotal--;
                if (unsentTotal === 0) { callback(); }
            });
        }
    };

    monUtils.loadStylesheet = function(url, callback) {

        // Is stylesheet already loaded? - Just do callback.
        $('link').each(function() { if ($(this).attr('href') === url) { callback(); } });

        var stylesheet = document.createElement('link');
        stylesheet.type = 'text/css';
        stylesheet.rel = 'stylesheet';
        
        if (stylesheet.readyState) {  //IE - TODO, may not be needed with edge.
            stylesheet.onreadystatechange = function () {
                if (stylesheet.readyState === 'loaded' ||
                        stylesheet.readyState === 'complete') {
                    stylesheet.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            stylesheet.onload = function () {
                callback();
            };
        }

        stylesheet.href = url;
        document.getElementsByTagName('head')[0].appendChild(stylesheet);
    };    
    
    // True if property is found, does not look at value of property (undefined, null, etc.)
    // Credit https://stackoverflow.com/questions/135448/how-do-i-check-if-an-object-has-a-property-in-javascript
    monUtils.objectHasProperty = function(obj, prop) {

        var proto = obj.__proto__ || obj.constructor.prototype;
        return (prop in obj) && (!(prop in proto) || proto[prop] !== obj[prop]);
    };

    monUtils.round = function(numberIn, places) {
        
        if (places == 0) {
            return Math.round(numberIn);
        } else {
            return Math.round(numberIn * places * 10) / places / 10;
        }
    };
    
    monUtils.kmToMi = function(kmIn) {

        return kmIn * 0.621371;
    };
    
    // This takes data with encoded html entities such as K&#299;lauea and returns Kīlauea
    // Thanks to https://stackoverflow.com/questions/7394748/whats-the-right-way-to-decode-a-string-that-has-special-html-entities-in-it
    monUtils.decodeHtml = function(html) {
    
        var txt = document.createElement('textarea');
        txt.innerHTML = decodeURI(html);
        return txt.value;
    };

    // Fired when onclick is included and bootstrap accordion header is clicked.
    monUtils.bsAcdnHeaderClicked = function(ele) {
        
        if (monUtils.preventAccordion) {
            monUtils.preventAccordion = false;
            return;
        }
        
        $($(ele).attr('data-parent')).find('.collapse').collapse('hide');
        $($(ele).attr('data-collapse-id')).collapse('toggle');
        
        if (ele.id === 'vscmmSettingsHeading') {
            monUI.setUIForDepthUnits();
            monUI.setUIForTimeUnits();
            monUI.setUIForQuakeColor();
        }
    };

    // Gets the path to the monitoring files.
    monUtils.getMonitoringPath = function() {
        
        var jsFileLocation = '';
        
        if ($('script[src*="monitoring.min"]').length > 0) {
            jsFileLocation = $('script[src*="monitoring.min"]').attr('src');  // the js file path
            jsFileLocation = jsFileLocation.replace('monitoring.min.js', '');   // the js folder path
        } else if ($('script[src*=monitoring-utils]').length > 0) {
            jsFileLocation = $('script[src*=monitoring-utils]').attr('src');  // the js file path
            jsFileLocation = jsFileLocation.replace('monitoring-utils-2.js', '');   // the js folder path
        }
        return jsFileLocation.replace('scripts/', '');   // the parent folder path
    };

    monUtils.clone=function(obj){
        var copy;
    
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;
    
        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
    
        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = monUtils.clone(obj[i]);
            }
            return copy;
        }
    
        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = monUtils.clone(obj[attr]);
            }
            return copy;
        }
    
        throw new Error("Unable to copy obj! Its type isn't supported.");
    }
    
    // The download function takes a CSV string, the filename and mimeType as parameters
    // Scroll/look down at the bottom of this snippet to see how download is called
    monUtils.downloadCSV = function(content, fileName, mimeType) {
      var a = document.createElement('a');
      mimeType = mimeType || 'application/octet-stream';
    
      if (navigator.msSaveBlob) { // IE10
        navigator.msSaveBlob(new Blob([content], {
          type: mimeType
        }), fileName);
      } else if (URL && 'download' in a) { //html5 A[download]
        a.href = URL.createObjectURL(new Blob([content], {
          type: mimeType
        }));
        a.setAttribute('download', fileName);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      } else {
        location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
      }
    }
    
}(window.monUtils = window.monUtils || {}, jQuery));

/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function ($) {

    // Detect touch support
    $.support.touch = 'ontouchend' in document;

    // Ignore browsers without touch support
    if (!$.support.touch) {
        return;
    }

    var mouseProto = $.ui.mouse.prototype,
            _mouseInit = mouseProto._mouseInit,
            _mouseDestroy = mouseProto._mouseDestroy,
            touchHandled;

    /**
     * Simulate a mouse event based on a corresponding touch event
     * @param {Object} event A touch event
     * @param {String} simulatedType The corresponding mouse event
     */
    function simulateMouseEvent(event, simulatedType) {

        // Ignore multi-touch events
        if (event.originalEvent.touches.length > 1) {
            return;
        }

        event.preventDefault();

        var touch = event.originalEvent.changedTouches[0],
                simulatedEvent = document.createEvent('MouseEvents');

        // Initialize the simulated mouse event using the touch event's coordinates
        simulatedEvent.initMouseEvent(
                simulatedType, // type
                true, // bubbles                    
                true, // cancelable                 
                window, // view                       
                1, // detail                     
                touch.screenX, // screenX                    
                touch.screenY, // screenY                    
                touch.clientX, // clientX                    
                touch.clientY, // clientY                    
                false, // ctrlKey                    
                false, // altKey                     
                false, // shiftKey                   
                false, // metaKey                    
                0, // button                     
                null              // relatedTarget              
                );

        // Dispatch the simulated event to the target element
        event.target.dispatchEvent(simulatedEvent);
    }
    
    // Below is the jQuery UI Touch Punch mod that enables the draggable mod to work with touch devices.
    // Read more here: http://touchpunch.furf.com/

    /**
     * Handle the jQuery UI widget's touchstart events
     * @param {Object} event The widget element's touchstart event
     */
    mouseProto._touchStart = function (event) {

        var self = this;

        // Ignore the event if another widget is already being handled
        if (touchHandled || !self._mouseCapture(event.originalEvent.changedTouches[0])) {
            return;
        }

        // Set the flag to prevent other widgets from inheriting the touch event
        touchHandled = true;

        // Track movement to determine if interaction was a click
        self._touchMoved = false;

        // Simulate the mouseover event
        simulateMouseEvent(event, 'mouseover');

        // Simulate the mousemove event
        simulateMouseEvent(event, 'mousemove');

        // Simulate the mousedown event
        simulateMouseEvent(event, 'mousedown');
    };

    /**
     * Handle the jQuery UI widget's touchmove events
     * @param {Object} event The document's touchmove event
     */
    mouseProto._touchMove = function (event) {

        // Ignore event if not handled
        if (!touchHandled) {
            return;
        }

        // Interaction was not a click
        this._touchMoved = true;

        // Simulate the mousemove event
        simulateMouseEvent(event, 'mousemove');
    };

    /**
     * Handle the jQuery UI widget's touchend events
     * @param {Object} event The document's touchend event
     */
    mouseProto._touchEnd = function (event) {

        // Ignore event if not handled
        if (!touchHandled) {
            return;
        }

        // Simulate the mouseup event
        simulateMouseEvent(event, 'mouseup');

        // Simulate the mouseout event
        simulateMouseEvent(event, 'mouseout');

        // If the touch interaction did not move, it should trigger a click
        if (!this._touchMoved) {

            // Simulate the click event
            simulateMouseEvent(event, 'click');
        }

        // Unset the flag to allow other widgets to inherit the touch event
        touchHandled = false;
    };

    /**
     * A duck punch of the $.ui.mouse _mouseInit method to support touch events.
     * This method extends the widget with bound touch event handlers that
     * translate touch events to mouse events and pass them to the widget's
     * original mouse event handling methods.
     */
    mouseProto._mouseInit = function () {

        var self = this;

        // Delegate the touch handlers to the widget's element
        self.element.bind({
            touchstart: $.proxy(self, '_touchStart'),
            touchmove: $.proxy(self, '_touchMove'),
            touchend: $.proxy(self, '_touchEnd')
        });

        // Call the original $.ui.mouse init method
        _mouseInit.call(self);
    };

    /**
     * Remove the touch event handlers
     */
    mouseProto._mouseDestroy = function () {

        var self = this;

        // Delegate the touch handlers to the widget's element
        self.element.unbind({
            touchstart: $.proxy(self, '_touchStart'),
            touchmove: $.proxy(self, '_touchMove'),
            touchend: $.proxy(self, '_touchEnd')
        });

        // Call the original $.ui.mouse destroy method
        _mouseDestroy.call(self);
    };

})(jQuery);
