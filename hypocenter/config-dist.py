"""Config file for the hypocenter application.

This file should be copied to config.py, then modified to suit your deployment.
"""

DB_HOST='you.db.host'
DB_USER='db.user'
DB_PASSWORD='db.password'
DB_NAME = 'db.name'
