import flask
import csv

from datetime import date

import requests
from dateutil.parser import parse
from . import app


@app.route("/report")
def report_idx():
    return flask.render_template("report.html")

@app.route('/report/run')
def run_report():
    date_from=parse(flask.request.args.get('datefrom',date.today())).strftime("%Y-%m-%dT00:00:00.000Z")
    date_to=parse(flask.request.args.get('dateto',date.today())).strftime("%Y-%m-%dT23:59:59.999Z")
    
    URL='https://earthquake.usgs.gov/fdsnws/event/1/query.geojson'
    args={'starttime':date_from,
          'endtime':date_to,
          'catalog':'av',
          'orderby':'time'}
    
    
    result=requests.get(URL,args)
    value=result.json()
    return flask.jsonify(value)
