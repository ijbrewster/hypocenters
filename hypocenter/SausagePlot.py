import os
import pickle
import time
import warnings

from datetime import datetime, timedelta, timezone
from functools import lru_cache, partial

import flask
import numpy as np
import pandas

from cachetools.func import ttl_cache
from dateutil.parser import parse
from pymysql.err import OperationalError

import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors

from . import app, utils


_AREA_RADIUS = {
    'littlesitkin': 12.00,
    'semisopochnoi': 7.00,
    'gareloi': 10.00,
    'tanaga': 4.00,
    'takawangha': 5.00,
    'bobrof': 2.00,
    'kanaga': 3.00,
    'moffett': 3.00,
    'greatsitkin': 6.00,
    'kliuchef': 9.00,
    'korovin': 4.00,
    'okmok': 10.00,
    'makushin': 12.00,
    'tabletopmtn': 2.00,
    'widebaycone': 2.00,
    'akutan': 7.00,
    'gilbert': 3.00,
    'westdahl': 7.00,
    'fisher': 10.00,
    'shishaldin': 5.00,
    'roundtop': 8.00,
    'dutton': 10.00,
    'hague': 8.00,
    'pavlof': 3.50,
    'pavlofsister': 2.00,
    'veniaminof': 17.00,
    'aniakchak': 14.00,
    'ukinrek': 7.00,
    'ugashikpeulik': 5.00,
    'martin': 3.00,
    'mageik': 3.50,
    # 'trident': 1.40,
    # 'novarupta': 1.00,
    'tridentnovarupta': 6.00,
    'katmai': 5.00,
    'griggs': 3.00,
    'snowy': 5.00,
    'denison': 2.00,
    'steller': 2.00,
    'kukak': 2.00,
    'devilsdesk': 2.50,
    'fourpeaked': 8.00,
    'douglas': 3.00,
    'augustine': 1.50,
    'iliamna': 7.00,
    'redoubt': 8.00,
    'spurr': 10.00,
    'wrangell': 20.00,
    'davidof': 8.00,
    'bobrof': 2.00,
    'moffett': 3.00,
    'tabletopmtn': 2.0,
    'widebaycone': 2.0,
    'gilbert': 3.0,
    'fisher': 10.0,
    'roundtop': 8.0,
    'denison': 2.0,
    'kukak': 2.0,
    'devilsdesk': 2.5,
    'douglas': 3.0,
    'katmaivolcaniccluster': 25.00,
    'bogoslof': 12.00,
    'edgecumbe': 10.00,
}

_REGIONS = [
    ['Katmai Volcanic Cluster', 58.2343, -155.1026],
    ['Trident/Novarupta', 58.2343, -155.1026]
]


@lru_cache()
def get_regions():
    QUERY = """SELECT
    volcano_name,
    latitude,
    longitude
    FROM volcano"""

    region_columns = ['place', 'lat', 'lon']

    with utils.GeodivaCursor() as cursor:
        cursor.execute(QUERY)
        res = pandas.DataFrame(cursor, columns = region_columns)

    other_regions = pandas.DataFrame(_REGIONS, columns = region_columns)
    res = pandas.concat([res, other_regions], ignore_index = True)

    radius = pandas.DataFrame.from_dict(_AREA_RADIUS, 'index', columns = ['radius'])
    # Create a column with place names matching our radius dataframe names
    res['radius_key'] = res['place'].str.replace(r'[\s-]+', '', regex = True).str.replace('/', '').str.lower()

    res = res.merge(radius, left_on = 'radius_key', right_index = True,
                    copy = False)

    del res['radius_key']

    return res


@ttl_cache(5)
def get_quakes(start_time, end_time = None, timeout = 15):
    # We don't need sub-minute precision on the events here,
    # so round to the nearest half-hour or so

    start_time = start_time.replace(minute = 0, second = 0, microsecond = 0)
    end_time = end_time.replace(minute = 0, second = 0, microsecond = 0)
    quake_df = utils.get_consoldated_events(start_time, end_time, timeout)
    if quake_df.size == 0:
        return quake_df

    # Drop the auto-locations
    quake_df.drop(quake_df[quake_df['auto'] == True].index, inplace = True)

    #Make sure we still have *something*
    if quake_df.size == 0:
        return quake_df

    # Get just the columns we care about
    quake_df = quake_df[['long', 'lat', 'depthKM', 'mag', 'unixTime']]

    # Add an energy column so we can get the sum of the energy per week/volcano
    quake_df = quake_df.assign(energy=np.power(10, 5.24 + 1.44 * quake_df['mag'].astype(float)))
    # quake_df.loc[:, 'energy'] = np.power(10, 5.24 + 1.44 * quake_df['mag'].astype(float))

    # Convert the timestamp column into real datetime objects
    quake_df['dt'] = pandas.to_datetime(quake_df['unixTime'], unit='ms', utc=True).dt.normalize()
    # quake_df.loc[:, 'dt'] = pandas.to_datetime(quake_df['unixTime'],
                                               # unit = 'ms', utc = True).dt.normalize()

    return quake_df


percentile_labels = [50, 60, 70, 80, 90, 100]


@ttl_cache(2, 24 * 60 * 60)  # Cache two week days in memory for 24 hours
def _get_percentile_levels(week_break, timeout = 15):
    """Calculate the percentile levels for all volcanos from the full dataset,
    so we don't have to pull the full dataset each time we query this page."""

    cache_path = os.path.dirname(__file__)
    cache_path = os.path.join(cache_path, '..',
                              f'OtherFiles/Cache/{week_break}.cache')
    cache_path = os.path.realpath(cache_path)
    try:
        age = time.time() - os.path.getmtime(cache_path)
        if age <= 60 * 60 * 24:
            # Load the file
            with open(cache_path, 'rb') as cache_file:
                percentiles = pickle.load(cache_file)

            return percentiles
        else:
            # File is too old. Delete it before moving on
            os.unlink(cache_path)
    except FileNotFoundError:
        pass  # Cache file doesn't exist yet. Move on.

    query_period = timedelta(days = 365 * 2)
    query_start = datetime.utcnow() - query_period
    query_end = datetime.utcnow()
    quake_df = None
    for i in range(5):
        result_df = get_quakes(query_start, query_end, timeout)
        if result_df.size == 0:
            break  # No more quakes to get

        if quake_df is None:
            quake_df = result_df
        else:
            quake_df = pandas.concat([quake_df, result_df])
            #quake_df = quake_df.append(result_df, ignore_index = True)
        query_start -= query_period
        query_end -= query_period

    print("Getting regions for percentiles")
    areas = get_regions()
    print("Calculating percentiles")

    grouper = pandas.Grouper(key = 'dt', freq = week_break)

    percentiles = {}

    for idx, row in areas.iterrows():
        lat1 = row.lat
        lon1 = row.lon
        radius = row.radius

        dists = utils.haversine_np(lon1, lat1, quake_df['long'], quake_df['lat'])
        place_filter = dists <= radius

        if place_filter.any():
            all_volc_quakes = quake_df.loc[place_filter]
            all_counts = all_volc_quakes.groupby(grouper)['unixTime']\
                .count().sort_values().reset_index()['unixTime']

            percentile_values = [0] + [all_counts.quantile(x / 100)
                                       for x in percentile_labels]

            # Fix any duplicates
            for i in range(1, len(percentile_values)):
                if percentile_values[i] <= percentile_values[i - 1]:
                    percentile_values[i] = percentile_values[i - 1] + .001  # just make it a hair bigger

            percentiles[row.place] = percentile_values

    print("Completed percentile calculations")
    os.makedirs(os.path.dirname(cache_path), exist_ok = True)
    with open(cache_path, 'wb') as cache_file:
        pickle.dump(percentiles, cache_file)
    return percentiles


@lru_cache(1)  # Only need to do this once, so might as well cache it.
def gen_colormap():
    #colormapname = 'RdYlGr_r'
    colormapname = 'hot_r'
    MAXMAGCOLORBAR = 5.0
    MINMAGCOLORBAR = 1.0
    mycolormap = cm = plt.get_cmap(colormapname)
    cNorm = colors.Normalize(vmin=MINMAGCOLORBAR, vmax=MAXMAGCOLORBAR)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=mycolormap)
    return scalarMap


def get_rgba_from_cm(cm, val):
    return (np.asarray(cm.to_rgba(val)) * 255).tolist()


@app.route('/weeklysummary/data')
def weekly_summary_data():
    number_of_weeks_to_plot = int(flask.request.args.get('weeks', 13))
    weeksagofilter = int(flask.request.args.get('filter', 1))
    page_num = int(flask.request.args.get('page', 0))
    end_date = flask.request.args.get('endDate')
    timeout = int(flask.request.args.get('timeout', 15))

    end_time = datetime.utcnow()
    if end_date:
        try:
            end_time = parse(end_date)
        except ValueError:
            return flask.abort(400)
        if end_time.tzinfo is None:
            end_time = end_time.replace(tzinfo = timezone.utc)

    if (number_of_weeks_to_plot < weeksagofilter):
        return "Filter should be <= weeks", 400

    page = timedelta(weeks = number_of_weeks_to_plot)

    if page_num > 0:
        end_time -= (page * page_num)

    start_time = end_time - page
    filter_start = end_time - timedelta(weeks = weeksagofilter) +\
        timedelta(days = 1)

    filter_start = filter_start.replace(tzinfo = timezone.utc, hour = 0,
                                        minute = 0, second = 0, microsecond = 0)

    quake_df = get_quakes(start_time, end_time, timeout)

    weekfilter = quake_df['dt'] >= filter_start

    # bool_plot_percentiles_figure = True if number_of_weeks_to_plot == weeksagofilter else False

    try:
        res = get_regions()
    except OperationalError:
        return "Unable to fetch list of volcanos from DB", 502

    results = {}

    week_break = f"W-{end_time.strftime('%a').upper()}"
    grouper = pandas.Grouper(key = 'dt', freq = week_break)
    percentiles = _get_percentile_levels(week_break, timeout)
    color_map = gen_colormap()
    cm_func = partial(get_rgba_from_cm, color_map)

    weeks = pandas.date_range(start_time.date(), end_time.date(),
                              freq = week_break, tz = timezone.utc)

    # should only be 40 or 50, so not too slow
    for idx, row in res.iterrows():
        lat1 = row.lat
        lon1 = row.lon
        radius = row.radius

        # Calculate the distance from each quake to this area
        dists = utils.haversine_np(lon1, lat1, quake_df['long'], quake_df['lat'])
        place_filter = dists <= radius

        # Only care about this volcano if it has quakes in the past weekfilter
        if (place_filter & weekfilter).any():
            volc_quakes = quake_df.loc[place_filter]
            try:
                percentile_values = percentiles[row.place]
            except KeyError:
                continue

            by_week = volc_quakes.groupby(grouper)

            # this gives me a count by week
            weekly_data = by_week['unixTime'].count().reset_index()
            weekly_data = weekly_data.set_index('dt').reindex(weeks,
                                                              copy = False)

            weekly_data = weekly_data.rename(columns = {'unixTime': 'count', })
            weekly_data['count'] = weekly_data['count']\
                .replace(np.nan, '0').astype(int)

            weekly_data['percentile'] = pandas.cut(weekly_data['count'],
                                                   bins = percentile_values,
                                                   labels = percentile_labels,
                                                   duplicates = 'drop').astype(float)

            # Fix percentiles over 100
            weekly_data.loc[weekly_data['count'] > max(percentile_values), 'percentile'] = 100

            weekly_data['percentile'] = weekly_data['percentile']\
                .replace(np.nan, 0).astype(int)

            energy = by_week['energy'].sum()

            weekly_data = weekly_data.join(energy)

            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", message="divide by zero encountered in log10")
                weekly_data['mag'] = (np.log10(weekly_data['energy']) - 5.24) / 1.44
            # Don't need this column any more
            weekly_data.drop(labels = 'energy', axis = 'columns', inplace = True)

            # Clean up out-of-range/missing values
            weekly_data.loc[weekly_data['mag']==-np.inf, 'mag'] = 0
            weekly_data.loc[weekly_data['mag'].isna(), 'mag'] = 0
            # weekly_data.mag.replace(-np.inf, 0, inplace = True)
            # weekly_data.mag.replace(np.nan, 0, inplace = True)
            weekly_data['color'] = weekly_data['mag'].map(cm_func)

            # Sort with newest week first
            weekly_data.sort_index(ascending = False, inplace = True)

            weekly_data['dt'] = weekly_data.index.strftime("%m/%d")

            total_quakes = weekly_data['count'].iloc[0]
            results[row.place] = {'lon': lon1, 'week_count': int(total_quakes),
                                  'week_data': weekly_data.to_dict("records")}

    sort_list = [(key, value['lon']) for key, value in results.items()]
    sort_list.sort(key = lambda x: x[1] if x[1] < 0 else x[1] - 360)
    ordered_places = tuple((x[0] for x in sort_list))
    date_range = weeks.sort_values(ascending = False).strftime('%m/%d/%Y').tolist()
    ret_obj = {'results': results,
               'weeks': weeks.sort_values(ascending = False).strftime('%m/%d').tolist(),
               'places': ordered_places,
               'from': date_range[-1],
               'to': date_range[0], }
    return flask.jsonify(ret_obj)


@app.route('/weeklysummary')
def weekly_summary():
    return flask.render_template('weeklysum.html')
