# import warnings
# warnings.simplefilter("error")
import os
import flask
os.environ['MPLCONFIGDIR'] = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))


app = flask.Flask(__name__)

from . import main, report, SausagePlot
