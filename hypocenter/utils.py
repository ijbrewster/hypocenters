import json
import pickle
import warnings
import traceback

from datetime import datetime, timedelta, timezone
from functools import wraps
from dateutil import tz

import numpy as np
import pandas
import pymysql
import requests

import psycopg2
from psycopg2.extras import RealDictCursor
import redis

from . import config, app


class CacheWarning(UserWarning):
    pass


class RedisDict:
    def __init__(self, ttl = 2592000):
        self._ttl = ttl

    def _hash(self, key):
        return json.dumps(
            key,
            ensure_ascii=False,
            sort_keys=True,
            indent=None,
            separators=(',', ':')
        )

    def _conn_redis(self):
        return redis.Redis(db = 1)

    def __len__(self):
        r = self._conn_redis()
        return len(r.keys())

    def __getitem__(self, key):
        hashkey = self._hash(key)

        r = self._conn_redis()
        if not hashkey.encode() in r.keys():
            raise KeyError(key)

        return pickle.loads(r.get(hashkey))

    def __setitem__(self, key, value):
        hashkey = self._hash(key)
        value = pickle.dumps(value)

        r = self._conn_redis()
        r.set(hashkey, value)
        r.expire(hashkey, self._ttl)

    def __delitem__(self, key):
        key = self._hash(key)
        r = self._conn_redis()
        r.delete(key)

    def __iter__(self):
        r = self._conn_redis()
        return (json.loads(x) for x in r.keys())

    def __contains__(self, item):
        item = self._hash(item)
        r = self._conn_redis()
        return item in r.keys()

    def get(self, key, default = None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def keys(self):
        r = self._conn_redis()
        keys = (json.loads(x) for x in r.keys())
        return tuple(keys)


class SafeTTLCache:
    _kwd_mark = object()

    def __init__(self, ttl = 300, key = None):
        self._ttl = timedelta(seconds = ttl)
        self._cache = RedisDict()
        self._size = 10
        # Keep a local list of keys, so we can delete
        # them in the order inserted if we get too big.
        self._keys = []
        self._f = None  # Initalize to nothing
        self._none_key = '(None, None)'
        self.key_func = key

    def __call__(self, f):
        self._f = f

        @wraps(f)
        def wrapped(*args, **kwargs):
            if self.key_func is not None:
                key = self.key_func(*args, **kwargs)
            else:
                key = str(args + tuple(sorted(kwargs.items())))

            cached = self._cache.get(key, {})
            age = datetime.now() - cached.get('timestamp', datetime.min)
            if age < self._ttl and 'data' in cached:
                app.logger.info("Returning cached result")
                return cached['data']

            # Either the cache doesn't exist, or it is too old. Run the function
            try:
                result = self._f(*args, **kwargs)
            except Exception as e:
                # If we get an exception when running the function, try to return
                # the previously cached value (if any). If no previous cache, just
                # re-raise the exception
                if not 'data' in cached:
                    raise

                # Cached value exists. Return it, but give a warning that we are doing so
                warnings.warn(f"Unable to refresh cache. Using stale data. Traceback:\n{traceback.format_exc()}",
                              CacheWarning)
                app.logger.error(f"Unable to refresh cache. Using stale data. Traceback:\n{traceback.format_exc()}")
                return cached['data']
            # Cache the result
            cache_entry = {'timestamp': datetime.now(),
                           'data': result}

            self._cache[key] = cache_entry
            self._keys.append(key)
            # Cleanup cache if too big
            if len(self._cache) > self._size:
                for key in self._keys:
                    # Keep the "none" key, as that is the big one
                    if key != self._none_key:
                        self._keys.remove(key)
                        del self._cache[key]
                        break  # only need to delete one
            return result
        return wrapped


class GeodivaCursor:
    def __init__(self):
        self.db_conn = None

    def __enter__(self):
        self.db_conn = pymysql.connect(host=config.DB_HOST, user=config.DB_USER,
                                       password=config.DB_PASSWORD, db=config.DB_NAME)
        return self.db_conn.cursor()

    def __exit__(self, t, v, traceback):
        if self.db_conn is not None:
            self.db_conn.close()


def haversine_np(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be of equal length.

    Less precise than vincenty, but fine for short distances,
    and works on vector math

    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat / 2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2.0)**2

    c = 2 * np.arcsin(np.sqrt(a))
    km = 6367 * c
    return km


def get_aqms_data(t_start = None, t_end = None):
    if t_start is None:
        starttime = datetime.utcnow() - timedelta(days = 366)
    else:
        starttime = t_start

    args = {'starttime': starttime.timestamp(), }

    query = """
    SELECT  e.evid as "eventId",
            truetime.true2nominalf(o.datetime) as "unixTime",
            lat,
            lon as "long",
            round(depth::numeric, 2) as "depthKM",
            (o.rflag='A') as auto,
            type,
            round(magnitude::numeric,2) as "mag",
            o.gap as "azimuthal-gap",
            ndef as "num-phases-used",
            erhor as "horizontal-error",
            sdep as "vertical-error",
            wrms as "rms"
        FROM event e
        INNER JOIN origin o ON o.orid=e.prefor
        INNER JOIN netmag n ON e.prefmag=n.magid
    WHERE o.datetime >= %(starttime)s
    """
    if t_end is not None:
        query += "AND o.datetime <= %(endtime)s"
        args['endtime'] = t_end.timestamp()

    query += """
    AND bogusflag=0
    AND selectflag=1
    AND o.rflag in ('A','F')
    ORDER BY datetime;
    """

    conn = psycopg2.connect(host = "avoaqmspp2.avo.alaska.edu", user = "browser",
                            password = "browser_pass", dbname = 'archdb1',
                            cursor_factory = RealDictCursor)
    cur = conn.cursor()
    cur.execute(query, args)
    results = cur.fetchall()
    return results


def comcatkey(*args, **kwargs):
    args = list(args)
    if 't_start' in kwargs:
        t_start = kwargs['t_start']
    else:
        t_start = args.pop(0)

    if 't_end' in kwargs:
        t_end = kwargs['t_end']
    else:
        t_end = args.pop(0)

    key = str((t_start, t_end))
    return key

class TooManyRequestsError(Exception):
    pass

def get_comcat_data(t_start = None, t_end = None, timeout = 15):
    # *roughly* one year back
    if t_start is None:
        starttime = datetime.utcnow() - timedelta(days = 366)
    else:
        starttime = t_start

    if t_end is None:
        t_end = datetime.utcnow() + timedelta(days = 1)

    if t_end - starttime > timedelta(weeks = 520):
        raise ValueError("Requested date range too large")

    # Break into chunks of no more than one month at a time

    date_range = pandas.date_range(start=starttime, end=t_end, freq='ME')
    if date_range.empty:
        date_range = pandas.DatetimeIndex([starttime])
    # Make sure the range is inclusive.
    if date_range[-1] < t_end:
        date_range = date_range.append(pandas.DatetimeIndex([t_end]))

    # Convert start and end times to strings
    date_range = date_range.strftime("%Y-%m-%dT00:00:00Z")

    events = {
        'type': 'FeatureCollection',
        'features': [],
        'metadata': {
            'count': 0,
        },
    }

    for start, end in  zip(date_range[:-1], date_range[1:]):
        try:
            resp_dict = retrieve_comcat_range(start, end, timeout)
            try:
                resp_dict = resp_dict.json()
            except AttributeError:
                pass

        except ValueError:
            continue
        except TooManyRequestsError:
            break

        events['features'].extend(resp_dict['features'])
        events['metadata']['count'] += resp_dict['metadata']['count']

    if events['metadata']['count'] == 0:
        raise ValueError("No results returned from query")
    return events


@SafeTTLCache(ttl = 86400, key = comcatkey)  # Store in local cache for 24 hours
def retrieve_comcat_range(start, end, timeout):
    args = {
        'starttime': start,
        'catalog': 'av',
        'orderby': 'time',
        'endtime': end,
    }

    url = f"https://earthquake.usgs.gov/fdsnws/event/1/query.geojson?"
    response = requests.get(url, args, timeout = timeout)
    if response.status_code != 200:
        app.logger.warning(f"Error retrieving event data. Server returned code {response.status_code}:\n{response.text.encode('ascii', 'ignore').decode()}")
        if response.status_code == 429:
            # Too many requests. No point in continuing -
            # we'll just keep getting the same error.
            raise TooManyRequestsError

        raise ValueError(f"Invalid response from server: {response.status_code} ({response.text})")

    return response.json()



def process_aqms_data(data):
    events = []
    for item in data:
        item = dict(item)
        item['eventId'] = f"av{item['eventId']}"
        item['date'] = datetime.fromtimestamp(item['unixTime'], tz = tz.UTC)
        item['unixTime'] *= 1000
        item['url'] = ''
        events.append(item)
    events = pandas.DataFrame(events)
    if events.size > 0:
        events.set_index('date', drop = False, inplace = True)

    return events


def process_comcat_data(data):
    events = [{
        'eventId': x['id'],
        'date': datetime.fromtimestamp(x['properties']['time'] / 1000, tz.UTC),
        'unixTime': x['properties']['time'],
        'url': x['properties']['url'],
        'mag': round(x['properties']['mag'], 2),
        'long': x['geometry']['coordinates'][0],
        'lat': x['geometry']['coordinates'][1],
        'depthKM': round(x['geometry']['coordinates'][2], 1),
        'auto': False,
        'rms': x['properties']['rms']
    } for x in data['features']
        if x['properties']['net'] == 'av' and x['properties']['mag'] is not None
    ]
    events = pandas.DataFrame(events)
    if events.size > 0:
        events.set_index('date', drop = False, inplace = True)
    return events


def get_consoldated_events(starttime = None, endtime = None, timeout = 15):
    # Make sure we are working in UTC
    if starttime is not None and starttime.tzinfo is None:
        starttime = starttime.replace(tzinfo = tz.UTC)
    if endtime is not None and endtime.tzinfo is None:
        endtime = endtime.replace(tzinfo = tz.UTC)

    try:
        aqms_data = get_aqms_data(starttime, endtime)
        aqms_events = process_aqms_data(aqms_data)
    except Exception as e:
        app.logger.exception(f"Unable to retrieve AQMS events: {e}")
        aqms_events = None

    try:
        usgs_events = get_comcat_data(starttime, endtime, timeout)
        events = process_comcat_data(usgs_events)
    except (ValueError, requests.exceptions.ReadTimeout) as e:
        app.logger.error(e)
        events = aqms_events

    if events is None or events.count == 0:
        return pandas.DataFrame()

    # Add in any missing events from the earthquakes.usgs.gov database
    if aqms_events is not None and aqms_events.size > 0:
        missing = set(events.eventId) - set(aqms_events.eventId)
        events = events[events.eventId.isin(missing)]
        events = pandas.concat([events, aqms_events])

    # make sure we don't have too much data
    if starttime:
        events = events.loc[(starttime <= events.index)]

    if endtime:
        events = events.loc[(endtime >= events.index)]

    # for good measure
    # Just in case there is some overlap in the queries.
    events.drop_duplicates('eventId', inplace = True)
    return events
