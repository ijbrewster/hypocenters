import csv
import flask
import json
import math

import pandas
import pymysql

from cachetools import TTLCache
from dateutil import parser, tz
from io import StringIO, BytesIO


import plotly.graph_objects as go
import plotly.io as pio
# Avoid mathjax loading imagery
pio.kaleido.scope.mathjax = None

from . import app, config, utils

quake_cache = TTLCache(256, 600)


@app.route("/")
def index():
    return flask.render_template("index.html")


@app.route('/getEvents')
def get_events():
    starttime = flask.request.args.get('startTime')
    if starttime:
        t_start = parser.parse(starttime).replace(tzinfo = tz.UTC)
    else:
        t_start = None

    endtime = flask.request.args.get('endTime')
    if endtime:
        t_end = parser.parse(endtime).replace(tzinfo = tz.UTC)
    else:
        t_end = None

    timeout = int(flask.request.args.get('timeout', "15"))
    events = utils.get_consoldated_events(t_start, t_end, timeout = timeout)

    # Check range of data to see if we have what we want
    if starttime and endtime:
        # filter data to just the range of interest
        events = events.loc[(t_start <= events.index) & (events.index <= t_end)]

        # See if we need to get any older data.
        # The dataset was initally populated as to-date,
        # so we will never need to get newer data
        if len(events) == 0 or t_start < events.index.min():
            if len(events) > 0:
                t_end = events.index.min()
            try:
                new_data = utils.get_consoldated_events(t_start, t_end, timeout = timeout)
            except:
                new_data = pandas.DataFrame()

            events = pandas.concat([new_data, events])

            # Just in case there is some overlap in the queries.
            events.drop_duplicates('eventId', inplace = True)

    return events.to_json(orient = "records")


@app.route('/distsFromPoint', methods = ['POST'])
def get_dists_from_point():
    print("Dists from point request received")
    origin = json.loads(flask.request.values['origin']) # [lat,lon]
    events = json.loads(flask.request.values['events'])
    data = [(x['eventId'], x['lat'], x['long']) for x in events]
    ids, lats, lons = zip(*data)
    dists = utils.haversine_np(lons, lats, origin[1], origin[0])
    result = {x[0]: x[1] for x in zip(ids, dists)}
    return flask.jsonify(result)


@app.route('/getDistanceOptions')
def get_dist_opts():
    try:
        db_conn = pymysql.connect(
            host=config.DB_HOST, user=config.DB_USER,
            password=config.DB_PASSWORD, db='hypocenters',
            connect_timeout=2 )
    except pymysql.err.OperationalError as e:
        app.logger.error(f"Unable to calculate distances: {e}")
        # Can't get a connection for some reason
        return []

    db_cursor = db_conn.cursor()

    QUERY = """
    SELECT
        Name,
        lat,
        lon
    FROM Landmarks
    ORDER BY Name
    """

    try:
        db_cursor.execute(QUERY)
        results = db_cursor.fetchall()
    finally:
        db_conn.close()

    response = [(json.dumps((x[1], x[2])), x[0]) for x in results]
    return flask.jsonify(response)


@app.route('/getDistances')
def get_distances():
    # exceptions should just be passed up to flask, and return an error to the user.
    # We might want to make this more elegant at some point.
    lat = flask.request.args['lat']
    lon = flask.request.args['lon']
    n = 3  # number of results desired
    return flask.jsonify(_get_distances(lat, lon, n))


def _get_distances(lat, lon, n):
    try:
        db_conn = pymysql.connect(
            host=config.DB_HOST, user=config.DB_USER,
            password=config.DB_PASSWORD, db='hypocenters',
            connect_timeout=2
        )
    except pymysql.err.OperationalError as e:
        app.logger.error(f"Unable to calculate distances: {e}")
        # Can't get a connection for some reason
        return []

    db_cursor = db_conn.cursor()

    QUERY = """SELECT
    Name,
    lat,
    lon,
    (6371 * acos( cos(radians(%s)) * cos( radians( lat )) * cos(radians( lon ) - radians(%s)) + sin( radians(%s)) * sin( radians(lat)))) AS distance
    FROM Landmarks
    HAVING distance < 1000
    ORDER BY distance LIMIT %s"""

    try:
        db_cursor.execute(QUERY, (lat, lon, lat, n))
        results = db_cursor.fetchall()
    finally:
        db_conn.close()

    if not results:
        flask.abort(404)

    distances = []
    for location in results:
        brng, direction = _get_direction(location[1], location[2], float(lat), float(lon))
        loc_dict = {
            'name': location[0],
            'dist_km': location[3],
            'lat': location[1],
            'lon': location[2],
            'bearing': brng,
            'direction': direction,
        }
        distances.append(loc_dict)
    return distances


def _get_direction(lat1, lon1, lat2, lon2):
    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    y = math.sin(dlon) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dlon)
    brng = math.degrees(math.atan2(y, x))
    if brng < 0:
        brng += 360
    compassidx = round((brng + 11.25) / 22.5)
    compass = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"]
    direction = compass[compassidx]

    return (brng, direction)


@app.route('/downloadQuakeData', methods=["POST"])
def download_quake_data():
    json_data = flask.request.form.get('quake_data')
    quake_list = json.loads(json_data)

    # Process the quake data to get distances (if needed)
    for quake in quake_list:
        distance = quake[-1]
        if distance is None:
            distance = _get_distances(quake[4], quake[5], 1)
            if distance:
                distance = distance[0]
            else:
                distance = None

        quake.append(distance or {'name': None, })  # May still be None, but oh well
        # See if we NOW have a distance record
        if distance is not None:
            km = distance['dist_km']
            mi = km * 0.621
            dist_string = f"{km:.2f} km ({mi:.2f} mi) {distance['direction']} of {distance['name']} ({distance['bearing']:.0f}º)"
            distance = dist_string

        quake[-2] = distance

    headers = ['Event ID', 'Date/Time UTC', 'Depth km', 'Magnitude', 'Latitude', 'Longitude', 'Distance From Closest Volcano']
    csv_file = StringIO()
    writer = csv.writer(csv_file)
    writer.writerow(headers)
    writer.writerows((x[:-1] for x in sorted(quake_list, key=lambda x: x[-1]['name'])))

    return flask.Response(csv_file.getvalue(),
                          mimetype="text/csv",
                          headers={"Content-disposition":
                                   "attachment; filename=earthquakes.csv"})


@app.route('/getPlotCSV', methods = ['POST'])
def gen_plot_csv():
    x_data = flask.request.form['x']
    y_data = flask.request.form['y']
    records = flask.request.form.get('records', '[]')
    x_data = json.loads(x_data)
    y_data = json.loads(y_data)
    records = json.loads(records)

    if not records:
        data = zip(x_data, y_data)
        headers = ['X', 'Y']
    else:
        data = []
        headers = ['X', 'Y', 'Event ID', 'Date/Time UTC', 'Depth km', 'Magnitude', 'Latitude', 'Longitude']
        for x, y, record in zip(x_data, y_data, records):
            data.append((
                x, y, record['eventId'], record['quakeUTCString'], record['depthKM'],
                record['mag'], record['lat'], record['long']
            ))

    csv_file = StringIO()
    writer = csv.writer(csv_file)
    writer.writerow(headers)
    writer.writerows(data)

    return flask.Response(csv_file.getvalue(),
                          mimetype = "text/csv",
                          headers = {'Content-disposition':
                                     "attachment; filename=plotdata.csv", })


@app.route('/getPlotPDF', methods = ["POST"])
def gen_plot_pdf():
    data = json.loads(flask.request.form['data'])

    # We get a line opacity attribute from Javascript that is apparently not applicable for python

    for trace in data:
        try:
            del trace['marker']['line']['opacity']
        except KeyError:
            pass

        try:
            del trace['records'] # Not a plotly dataset
        except KeyError:
            pass

    layout = json.loads(flask.request.form['layout'])

    fig = go.Figure(data = data, layout = layout)

    img = BytesIO()
    fig.write_image(img, format = 'pdf')
    img.seek(0)

    return flask.send_file(
        img,
        as_attachment = True,
        download_name = "hypocenters_plot.pdf",
        mimetype = "application/pdf"
    )
