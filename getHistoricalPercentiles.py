import os
import pickle

from datetime import datetime, timedelta, timezone, UTC

import pandas

from hypocenter.SausagePlot import get_regions
from hypocenter import utils


def update_percentile_cache():
    week_break = f"W-{datetime.today().strftime('%a').upper()}"

    query_period = timedelta(days = 60)
    query_start = datetime.now(UTC) - query_period
    query_end = datetime.now(UTC)
    fetch_end = datetime.min.replace(tzinfo = timezone.utc)
    quake_df = None
    cache_path = os.path.dirname(__file__)
    quake_path = os.path.join(cache_path, 'OtherFiles/Cache/all_quakes.gz')
    quake_path = os.path.realpath(quake_path)
    cache_path = os.path.join(cache_path, f'OtherFiles/Cache/{week_break}.cache')
    cache_path = os.path.realpath(cache_path)

    # auth = requests.auth.HTTPBasicAuth('internalavo', 'volcs4avo')
    percentile_labels = [50, 60, 70, 80, 90, 100]

    # See if we have an existing dataframe to load
    try:
        loaded = pandas.read_pickle(quake_path)
    except FileNotFoundError:
        pass
    else:
        quake_df = loaded
        fetch_end = quake_df['dt'].max().to_pydatetime()

    while True:
        res_df = utils.get_consoldated_events(query_start, query_end)

        if not res_df.size:
            break

        # rename columns to match expectations
        res_df.rename(columns = {
            'date': 'dt',
            'lat': 'Lat',
            'long': 'Lon',
            'eventId': 'DbId'
        },
            inplace = True)

        # Get rid of columns we don't need to save space
        res_df = res_df.loc[:, ['dt', 'Lat', 'Lon', 'DbId']]

        # Make the dt column be a pandas datetime
        res_df['dt'] = pandas.to_datetime(res_df['dt'], utc = True)

        # The index is redundant
        res_df.reset_index(drop = True, inplace = True)

        # Convert database ID records to integers
        res_df['DbId'] = res_df['DbId'].str.replace('av', '').astype('int64')

        print(res_df)
        if quake_df is None:
            quake_df = res_df
        else:
            quake_df = pandas.concat([quake_df, res_df], ignore_index = True)

        if query_start <= fetch_end:
            break  # We have enough data

        query_start -= query_period
        query_end -= query_period

    # Remove any duplicates created by overlap
    # Keep=last, since any changes to a record will show up at the end of the
    # list
    quake_df.drop_duplicates(subset = ['DbId'], keep = 'last', inplace = True)
    quake_df.sort_values('dt', inplace = True)
    # Save our dataframe to a pickle so we can simply update it next time
    quake_df.to_pickle(quake_path)

    percentiles = {}
    print("Getting regions for percentiles")
    areas = get_regions()
    print("Calculating percentiles")
    grouper = pandas.Grouper(key = 'dt', freq = week_break)
    for idx, row in areas.iterrows():
        lat1 = row.lat
        lon1 = row.lon
        radius = row.radius

        dists = utils.haversine_np(lon1, lat1, quake_df['Lon'], quake_df['Lat'])
        place_filter = dists <= radius

        if place_filter.any():
            all_volc_quakes = quake_df.loc[place_filter]

            # This gives the per-week count, sorted by count
            all_counts = all_volc_quakes.groupby(grouper).count()\
                .sort_values(by = 'Lat').reset_index()['Lat']

            percentile_values = [0] + [all_counts.quantile(x / 100)
                                       for x in percentile_labels]

            # Fix any duplicates
            for i in range(1, len(percentile_values)):
                if percentile_values[i] <= percentile_values[i - 1]:
                    percentile_values[i] = percentile_values[i - 1] + .001  # just make it a hair bigger

            percentiles[row.place] = percentile_values

    print("Completed percentile calculations")
    with open(cache_path, 'wb') as cache_file:
        pickle.dump(percentiles, cache_file)


if __name__ == "__main__":
    update_percentile_cache()
